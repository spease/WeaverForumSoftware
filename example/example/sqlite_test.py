#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .settings import *  # noqa F403

DEBUG = False

PASSWORD_HASHERS = ("django.contrib.auth.hashers.MD5PasswordHasher",)

LOGGING = {}

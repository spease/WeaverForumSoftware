# Weaver Forum Software

[![pipeline status](https://gitlab.com/spease/WeaverForumSoftware/badges/master/pipeline.svg)](https://gitlab.com/spease/WeaverForumSoftware/commits/master)
[![license BSD](https://img.shields.io/badge/license-BSD-lightgrey.svg)](https://gitlab.com/spease/WeaverForumSoftware/blob/master/LICENSE)
[![coverage report](https://gitlab.com/spease/WeaverForumSoftware/badges/master/coverage.svg)](https://gitlab.com/spease/WeaverForumSoftware/commits/master)
[![demo link](https://img.shields.io/badge/demo-available-brightgreen.svg)](https://weaverforum.com)
[![weaver pun](https://img.shields.io/badge/forum%20threads-weaved-brightgreen.svg)](https://weaverforum.com)

*Weaver Forum Software* is a lightweight, cutomizable forum solution
designed to run with minimal processing power, space, and bandwidth requirements.
It's intentionally designed to run with thousands of users even on the cheapest
Virtual Private Server.

Lightning fast for the end-user, WFS is also scalable and customizable with a
full-featured plugin system if you outgrow your small VPS or decide you want more features.

## Notable Features

* Super-lightweight installation
* Easy setup and deployment
* Per-Forum and Per-Rank permissions
* Full graceful fallback for all features if an enduser's javascript is disabled.
* User Rank flair and auto-promotion
* Admin Panel for ease of administration
* Theming and customization
* Full-featured plugin system
* Reports and Moderator Panel
* Thread pinning/locking/bumping
* Supports SQLite3 or PostgreSQL
* Custom BBCode support
* Full text search
* Native mobile support
* Fully locked down Content Security Policy to prevent cross-site scripting, clickjacking, and code injection attacks.


## Initial startup instructions:

**Fresh Install with SQLite3:**

```sh
# pip install weaverforum
git clone git@gitlab.com:spease/WeaverForumSoftware.git
pip install . # Not available on pypi yet

weaveradmin fresh-install
cd weaverforum
python3 manage.py runserver
```


At this point, you can access your quickstarted WFS forum at http://localhost:8000/
Login with the credentials for your superuser you just made, and then start creating your forum under Control Panel -> Admin Control Panel.


Of particular, you'll want to note "Forum Configuration," "Ranks," and "Forums."


Rank permissions are the 'default' permissions for that rank. Each forum, however, can have its own permission settings for each rank. Manage these carefully!


**Quickstart with SQLite3 into an existing project**

```sh
# pip install weaverforum
git clone git@gitlab.com:spease/WeaverForumSoftware.git
pip install . # Not available on pypi yet

weaveradmin existing-install <path-to-manage.py>
```

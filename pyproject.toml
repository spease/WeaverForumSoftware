[build-system]
requires = ["setuptools"]
build-backend = "setuptools.build_meta"

[project]
name = "WeaverForum"
authors = [
    {name = "Steffan Pease", email = "pease.47@osu.edu"},
]
maintainers = [
    {name = "Steffan Pease", email = "pease.47@osu.edu"},
]
description = "A lightweight, cutomizable forum solution designed to run with minimal processing power, space, and bandwidth."
readme = "README.md"
requires-python = ">=3.9"
keywords = ["django", "forum", "lightweight"]
classifiers = [
    "Development Status :: 3 - Alpha",
    "Natural Language :: English",
    'Environment :: Web Environment',
    'Framework :: Django',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: Message Boards',
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "Programming Language :: Python :: 3.13",
]

dependencies = [
    "Django>=4.1.2",
]

dynamic = ["version"]

[project.optional-dependencies]
dev = [
    "coverage",
    "isort",
    "django-debug-toolbar",
    "factory-boy",
    "bandit",
    "safety",
    "tzdata",
    "pylint",
    "black",
    "pre-commit",
]
docs = [
    "mkdocs",
]

[project.urls]
Homepage = "https://weaverforum.com"
Documentation = "https://gitlab.com/spease/WeaverForumSoftware"
GitLab = "https://gitlab.com/spease/WeaverForumSoftware.git"
Issues = "https://gitlab.com/spease/WeaverForumSoftware/-/issues"

[tool.setuptools.dynamic]
version = {attr = "weaverforum.__version__"}

[project.scripts]
weaveradmin = "weaverforum.cli:main"

[tool.isort]
line_length = 110
sections = ["FUTURE", "STDLIB", "FIRSTPARTY", "LOCALFOLDER", "THIRDPARTY"]
import_heading_stdlib = "Standard Library"
import_heading_firstparty = "Weaver Libraries"
import_heading_thirdparty = "External Dependencies"
balanced_wrapping = true
profile = "black"

[tool.pylint]
disable = ['wrong-import-order', 'missing-module-docstring', 'missing-class-docstring', 'missing-function-docstring',
'no-member']
ignore-paths = '^.*migrations/.*$'

[tool.pylint.FORMAT]
max-line-length = 110

[tool.pylint.DESIGN]
max-attributes = 10
max-args = 6
ignored-argument-names = "_.*|^ignored_|^unused_|args|kwargs|image"

[tool.black]
line-length = 110
exclude = '''
/(
    \.git
  | \.hg
  | \.mypy_cache
  | \.tox
  | \.venv
  | _build
  | buck-out
  | build
  | dist
  | migrations
  | migrations
)/
'''

[tool.coverage.run]
omit = [
    '*/plugins/*',
    '*/__init__.py',
    '*manage.py',
    '*settings*.py',
    '*urls.py',
    '*admin.py',
    '*/migrations/*',
    '*/tests/*',
    '*test_*.py',
    '*tests.py',
    '*wsgi.py',
    '*conftest.py',
    '*/extra*',
]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import logging
import re
import traceback
import zoneinfo
from datetime import datetime, timedelta
from html import escape

# Weaver Libraries
from weaverforum.core.embbroider.embbroider import emBBroider
from weaverforum.plugins.models import Plugin
from weaverforum.threads.models import Thread

# External Dependencies
from django import template
from django.urls import resolve, reverse
from django.utils import timezone
from django.utils.safestring import mark_safe

logger = logging.getLogger("django")

register = template.Library()


@register.simple_tag(takes_context=True)
def user_theme(context):
    """Given a user, returns the theme url."""
    if context["request"].user.is_authenticated:
        theme = context["profile"].theme
        if theme:
            return theme.get_filename()
    return context["settings"].default_theme.get_filename()


@register.simple_tag(takes_context=True)
def fdate(context, value):  # pylint: disable=too-many-return-statements
    """
    Takes a standard database time and converts it into the user's timezone for display.
    """
    if context["request"].user.is_authenticated:
        tz = zoneinfo.ZoneInfo(context["profile"].timezone)
    else:
        tz = zoneinfo.ZoneInfo(context["settings"].default_timezone)

    now = timezone.now().astimezone(tz)
    then = value.astimezone(tz)

    sod = datetime(now.year, now.month, now.day, hour=0, minute=0, second=0, tzinfo=now.tzinfo)
    soy = sod - timedelta(24)
    diff = now - then

    if diff.days == 0:
        if diff.seconds < 60:
            return "Just Now"
        if diff.seconds < 3600:  # 1 hour
            minutes = int(diff.seconds / 60)
            s = "s" if minutes > 1 else ""
            return f"{minutes} Minute{s} Ago"
        if diff.seconds < 43200:  # 12 hours
            hours = int(diff.seconds / 60 / 60)
            s = "s" if hours > 1 else ""
            return f"{hours} Hour{s} Ago"
        if then > sod:
            return f"Today at {then.strftime('%-I:%M%p')}"
        return f"Yesterday at {then.strftime('%-I:%M%p')}"
    # more than 24 hours ago
    if then > soy:
        return f"Yesterday at {then.strftime('%-I:%M%p')}"
    if diff.days < 365:
        return then.strftime("%b %-d, %-I:%M%p")
    return then.strftime("%b %-d, %Y")


@register.simple_tag(takes_context=True)
def avatar(context, user):
    """
    Takes an email and returns the URL of that email's avatar.
    """
    if user:
        url = user.profile.avatar_url
        if context["settings"].avatar_max_size <= 0 or context["settings"].avatar_max_dimensions <= 0:
            return ""
        if url:
            return mark_safe(f'<img class="avatar" src="{url}" alt/>')  # nosec
        return mark_safe('<span class="avatar noimage"></span>')  # nosec
    return mark_safe('<span class="avatar noimage"></span>')  # nosec


@register.simple_tag(takes_context=True)
def can_do(context, forum, permission, obj=None):
    if forum:
        return forum.can_do(context["user"], permission, obj)
    return False  # pragma: no cover


@register.filter
def truncatesearch(text, searchvalue):
    """
    We're given a post text in text. Given a search value, truncate that to a smaller 'preview'
    with the included word enclosed in [mark][/mark] tags.
    """
    text = escape(text).replace("\n", " ").replace("\r", "")
    searchvalue = escape(searchvalue)
    if not searchvalue:
        return mark_safe(text[:512])  # nosec
    locations = [m.start() for m in re.finditer(searchvalue.lower(), text.lower())]
    texts = []
    for location in locations:
        before_text = " ".join(text[:location].split(" ")[-14:])
        after_text = " ".join(text[location + len(searchvalue) :].split(" ")[:14])
        during_text = text[location : location + len(searchvalue)]
        texts.append(f"{before_text} <mark>{during_text}</mark> {after_text}")
    return mark_safe("<br /> ... <br />".join(texts))  # nosec


@register.simple_tag(takes_context=True)
def bbcode(context, text, tag_id):
    parser = emBBroider()
    return mark_safe(parser.parse(text, tag_id, context["subs"], extra_tags=context["tags"]))  # nosec


@register.simple_tag(takes_context=True)
def title_sub(context, text, clear=False):
    subs = context["subs"]
    if clear:
        return mark_safe(subs["N"].sub(lambda match: "", text))  # nosec
    return mark_safe(subs["N"].sub(lambda match: subs["ND"][match.group(0)], text))  # nosec


@register.simple_tag
def pprint_view(url):
    html = ""
    if url.startswith(reverse("admin:index")):
        html = "Admin Control Panel"
    else:
        try:
            resolved = resolve(url.split("?")[0].split("#")[0])
            msg, is_url = resolved.func.pprint_view
            if callable(msg):
                msg = msg(resolved.kwargs)
        except Exception:  # pylint: disable=broad-exception-caught
            # We want this to be a catchall in case something odd happens.
            logger.warning("[%s] %s", url, traceback.format_exc())
            msg = "Searching the Vast Abyss of Space"
            is_url = False
        if is_url:
            html = f'<a href="{url}">{msg}</a>'
        else:
            html = msg
    return mark_safe(html)  # nosec


@register.simple_tag(takes_context=True)
def get_last_thread(context, forum):
    try:
        forum_filter = set(forum.get_depth_children(depth=True).by_rank(context["rank"].id)) | {forum}
        last_thread = (
            Thread.objects.select_related("last_post_by")
            .filter(forum__in=forum_filter)
            .exclude(state=1)
            .latest("last_post_on")
        )
        return last_thread
    except Thread.DoesNotExist:
        return None


@register.simple_tag(takes_context=True)
def get_children(context, forum):
    return forum.children.by_rank(context["rank"].id)


@register.simple_tag(takes_context=True)
def get_plugins(context, section_name):
    response = ""
    for plugin_template in Plugin.cached_templates.get(section_name, ""):
        response += "\n" + plugin_template.render(context.flatten())
    return mark_safe(response)  # nosec

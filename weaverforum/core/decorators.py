#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
from functools import wraps

# Weaver Libraries
from weaverforum.core.models import ForumConfiguration

# External Dependencies
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.http import Http404


def pprint_view(return_val, url=False):
    """
    Decorator that allows you to annotate a view with a 'pretty print' description of that view.
    Used for translating URLs into messages for Currently Online.
    """

    def decorator(view):
        @wraps(view)
        def wrapper(request, *args, **kwargs):
            return view(request, *args, **kwargs)

        setattr(wrapper, "pprint_view", (return_val, url))
        return wrapper

    return decorator


def staff_admin_required(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_staff:
            return function(request, *args, **kwargs)
        raise Http404

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def guest_or_login_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in if ALLOW_GUEST_READ is false,
    redirecting to the log-in page if necessary.
    """
    try:
        allow_guest_read = ForumConfiguration.load().allow_guest_read
    except AttributeError:  # pragma: no cover
        allow_guest_read = False
    actual_decorator = user_passes_test(
        lambda u: (allow_guest_read or (u.is_authenticated and not allow_guest_read)),
        login_url=login_url,
        redirect_field_name=redirect_field_name,
    )
    if function:
        return actual_decorator(function)
    return actual_decorator  # pragma: no cover

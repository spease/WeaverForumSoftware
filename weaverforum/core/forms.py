#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.core import avatar
from weaverforum.core.models import ForumConfiguration

# External Dependencies
from django import forms
from django.forms.utils import ErrorList
from django.forms.widgets import Textarea
from django.utils.html import format_html, format_html_join, html_safe
from django.utils.safestring import mark_safe


@html_safe
class ErrorLabel(ErrorList):  # pylint: disable=too-few-public-methods
    def __str__(self):
        if not self.data:
            return ""
        return format_html(
            '<div class="{}">{}</div>',
            self.error_class,
            format_html_join(mark_safe("<br />"), "{}", ((e,) for e in self)),  # nosec
        )


class WeaverForm(forms.Form):  # pylint: disable=too-few-public-methods
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("label_suffix", "")
        kwargs.setdefault("error_class", ErrorLabel)
        super().__init__(*args, **kwargs)

    def as_div(self):
        "Return this form rendered as HTML <div>s, with the label AFTER the input."
        return self.render("forums/div.html")


class PostArea(Textarea):  # pylint: disable=too-few-public-methods
    template_name = "threads/postarea.html"


class AvatarField(forms.FileField):
    def clean(self, *args, **kwargs):
        data = super().clean(*args, **kwargs)

        if not data or not data.file:
            return data
        fc = ForumConfiguration.load()
        max_size = fc.avatar_max_size
        max_hw = fc.avatar_max_dimensions
        if max_size <= 0 or max_hw <= 0:
            return None

        try:
            image = avatar.FileChecker(max_size * 1000)
            image.open(data.file)
            image.get_metadata()
            image.verify()
        except avatar.ImageTooLarge as e:
            raise forms.ValidationError(f"Please keep the filesize under {max_size}kb.") from e
        except avatar.UnknownImageFormat as e:
            raise forms.ValidationError("Please upload a BMP, JPG, GIF, or PNG file.") from e
        if image.width > max_hw or image.height > max_hw:
            raise forms.ValidationError(f"Please upload something {max_hw}x{max_hw} or smaller.")
        return data

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        attrs["accept"] = ".png,.jpg,.jpeg,.bmp,.gif"
        return attrs

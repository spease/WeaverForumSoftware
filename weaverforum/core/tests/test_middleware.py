#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# External Dependencies
from django.contrib.auth.models import User
from django.test import TestCase, modify_settings


class CurrentlyOnlineMiddleware(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("weaver", "weaver@example.com", "password")

    @modify_settings(MIDDLEWARE={"prepend": "weaverforum.core.middleware.CurrentlyOnlineMiddleware"})
    def test_currently_online(self):
        resp = self.client.get("/")
        request = resp.wsgi_request
        self.assertTrue(len(request.currently_online) == 1)
        self.assertTrue(self.user == request.currently_online[0]["user"])

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.forums.models import Rank

# External Dependencies
import factory
import factory.django


class RankFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = Rank

    name = factory.Faker("word")
    display_title = factory.Faker("word")
    order_val = 100
    type = "D"

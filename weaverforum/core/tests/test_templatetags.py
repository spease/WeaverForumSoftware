#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import zoneinfo
from datetime import datetime, timedelta
from unittest.mock import patch

# Weaver Libraries
from weaverforum.models import Forum, ForumConfiguration, Thread

# External Dependencies
from django.contrib.auth.models import AnonymousUser, User
from django.template import Context, Template
from django.test import RequestFactory, TestCase
from django.utils import timezone


class TemplateTagTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user("weaver", "weaver@example.com", "password")
        self.client.login(username="weaver", password="password")  # nosec
        self.user.profile.theme_id = 1
        self.user.save()
        self.request = self.factory.get("/")
        self.request.user = self.user
        self.forum = Forum.objects.create(name="Test Forum", order=0)

    def test_user_theme(self):
        context = Context({"request": self.request, "profile": self.user.profile})
        template_to_render = Template("{% user_theme %}")
        rendered_template = template_to_render.render(context)
        self.assertInHTML("/static/weaver.css", rendered_template)

    def test_avatar(self):
        fc = ForumConfiguration.load()
        context = {
            "request": self.request,
            "settings": fc,
            "user": self.user,
        }
        template_to_render = Template("{% avatar user %}")
        rendered_template = template_to_render.render(Context(context))
        self.assertInHTML('<span class="avatar noimage"></span>', rendered_template)

        with patch.object(fc, "avatar_max_size", 0):
            template_to_render = Template("{% avatar user %}")
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("", rendered_template)

        class FakeAvatar:  # pylint: disable=too-few-public-methods
            pass

        fake = FakeAvatar()
        setattr(fake, "name", "test_url")
        self.user.profile.avatar = fake
        template_to_render = Template("{% avatar user %}")
        rendered_template = template_to_render.render(Context(context))
        self.assertInHTML('<img class="avatar" src="/media/test_url" alt/>', rendered_template)

        template_to_render = Template("{% avatar False %}")
        rendered_template = template_to_render.render(Context(context))
        self.assertInHTML('<span class="avatar noimage"></span>', rendered_template)

    def test_fdate(self):
        fc = ForumConfiguration.load()
        tz = zoneinfo.ZoneInfo(fc.default_timezone)

        # picked more or less at random
        template_to_render = Template("{% fdate time %}")

        with patch.object(timezone, "now", return_value=datetime(2024, 1, 3, 20, 35, 44).astimezone(tz)):
            now = timezone.now()
            context = {
                "request": self.request,
                "time": now,
                "settings": fc,
                "profile": self.user.profile,
            }

            context["time"] = now - timedelta(seconds=15)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("Just Now", rendered_template)

            context["time"] = now - timedelta(minutes=15)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("15 Minutes Ago", rendered_template)

            context["time"] = now - timedelta(minutes=65)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("1 Hour Ago", rendered_template)

            context["time"] = now - timedelta(hours=14)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("Today at 1:35AM", rendered_template)

            context["time"] = now - timedelta(hours=18)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("Yesterday at 9:35PM", rendered_template)

            context["time"] = now - timedelta(hours=25)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("Yesterday at 2:35PM", rendered_template)

            context["time"] = now - timedelta(days=65)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("Oct 30, 3:35PM", rendered_template)

            context["time"] = now - timedelta(days=400)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("Nov 29, 2022", rendered_template)

            self.request.user = AnonymousUser()
            context["time"] = now - timedelta(minutes=15)
            rendered_template = template_to_render.render(Context(context))
            self.assertInHTML("15 Minutes Ago", rendered_template)
            self.request.user = self.user

    def test_truncatesearch(self):
        context = Context({"request": self.request, "post": "test all the values"})
        template_to_render = Template('{% with text=post|truncatesearch:"test" %}{{ text }}{% endwith %}')
        rendered_template = template_to_render.render(context)
        self.assertInHTML("<mark>test</mark> all the values", rendered_template)

        template_to_render = Template('{% with text=post|truncatesearch:"" %}{{ text }}{% endwith %}')
        rendered_template = template_to_render.render(context)
        self.assertInHTML("test all the values", rendered_template)

    def test_pprint_view(self):
        context = Context({"request": self.request})
        template_to_render = Template(
            "{% for online in request.currently_online %}{%pprint_view online.action %}{% endfor %}"
        )
        rendered_template = template_to_render.render(context)
        self.assertInHTML("Admin Control Panel", rendered_template)

    def test_get_last_thread(self):
        context = Context(
            {
                "request": self.request,
                "forum": self.forum,
                "rank": self.user.profile.rank,
            }
        )
        template_to_render = Template("{% get_last_thread forum %}")
        rendered_template = template_to_render.render(context)
        self.assertInHTML("None", rendered_template)

        Thread.objects.create(forum=self.forum, title="Test Thread", state=0, created_by=self.user)
        rendered_template = template_to_render.render(context)
        self.assertInHTML("Test Thread", rendered_template)

    def test_get_children(self):
        context = Context(
            {
                "request": self.request,
                "forum": self.forum,
                "rank": self.user.profile.rank,
            }
        )
        template_to_render = Template("{% get_children forum %}")
        rendered_template = template_to_render.render(context)
        self.assertInHTML("&lt;ForumQuerySet []&gt;", rendered_template)

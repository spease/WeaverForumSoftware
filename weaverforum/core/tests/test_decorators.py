#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import logging

# External Dependencies
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse


class StaffAdminRequiredTest(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.user = User.objects.create_user("weaver", "weaver@example.com", "password")
        self.admin_user = User.objects.create_user(
            "weaver_admin", "weaver@example.com", "password", is_superuser=True
        )

    def test_not_staff(self):
        self.client.login(username="weaver", password="password")  # nosec
        response = self.client.get(reverse("admin:login"))
        self.assertEqual(response.status_code, 200)

    def test_is_staff(self):
        self.client.login(username="weaver_admin", password="password")  # nosec
        response = self.client.get(reverse("admin:login"))
        self.assertEqual(response.status_code, 302)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import logging

# Weaver Libraries
from weaverforum.core.embbroider.embbroider import emBBroider
from weaverforum.core.models import BBcodeTag, Substitution

# External Dependencies
from django.test import TestCase


class TreeTests(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        BBcodeTag.objects.create(name="test_extra_tags", is_solo_tag=True, html="<div></div>")
        self.bb = BBcodeTag.load()
        self.subs = Substitution.load()
        self.e = emBBroider()

    def test_bold_tree(self):
        parsed = self.e.parse("[b][i]test[/i][/b]", 0, self.subs, extra_tags=self.bb)
        self.assertEqual(parsed, "<b><em>test</em></b>")

    def test_tab(self):
        parsed = self.e.parse(
            "[tabs][tab=first]test[/tab][tab=second]test[/tab][/tabs]",
            0,
            self.subs,
            extra_tags=self.bb,
        )
        tabs = (
            '<br /><div class="bb_tab">'
            '<input class="tabinput" id="bb_tabtag_0_0" type="radio" name="tabs"  checked>\n'
            '<label class="tablabel" for="bb_tabtag_0_0">first</label>'
            '<div class="tabpanel"><p>test</p></div>\n'
            '<input class="tabinput" id="bb_tabtag_0_1" type="radio" name="tabs" >\n'
            '<label class="tablabel" for="bb_tabtag_0_1">second</label>'
            '<div class="tabpanel"><p>test</p></div></div><br />'
        )
        self.assertEqual(parsed, tabs)

    def test_toc_anchor(self):
        parsed = self.e.parse("[toc][tocanchor=test]text[/tocanchor]", 0, self.subs, extra_tags=self.bb)
        self.assertEqual(
            parsed,
            (
                '<div class="bb_toc">'
                '<b>Table of Contents</b><br /><span class="bb_toc_link">'
                '<a href="#a_test">text</a></span>'
                "</div><br /><br />"
                '<a href="#a_test" id="a_test" class="bb_tocanchor">text</a>'
            ),
        )

    def test_quote(self):
        parsed = self.e.parse("[quote=foo]a[/quote]", 0, self.subs, extra_tags=self.bb)
        self.assertEqual(parsed, "<blockquote>a<br />-foo</blockquote>")
        parsed = self.e.parse("[quote]a[/quote]", 0, self.subs, extra_tags=self.bb)
        self.assertEqual(parsed, "<blockquote>a</blockquote>")

    def test_spoiler_toggle(self):
        parsed = self.e.parse("[spoiler=b]s[/spoiler]", 0, self.subs, extra_tags=self.bb)
        t = '<details><summary class="spoiler">Spoiler: b</summary>s</details>'
        self.assertEqual(parsed, t)

    def test_toggle_toggle(self):
        parsed = self.e.parse("[toggle=b]s[/toggle]", 0, self.subs, extra_tags=self.bb)
        t = "<details><summary>b</summary>s</details>"
        self.assertEqual(parsed, t)

    def test_code(self):
        parsed = self.e.parse("[code][spoiler=foo]s[/spoiler][/code]", 0, self.subs, extra_tags=self.bb)
        t = "<code>[spoiler=foo]s[/spoiler]</code>"
        self.assertEqual(parsed, t)

    def test_graceful_fail(self):
        parsed = self.e.parse("[center=d]s[center]", 0, self.subs, extra_tags=self.bb)
        t = "[center=d]s[center]"
        self.assertEqual(parsed, t)

    def test_text(self):
        parsed = self.e.parse("[url=sfdfsdflem]sfdsf[/url]", 0, self.subs, extra_tags=self.bb)
        t = "[url=sfdfsdflem]sfdsf[/url]"
        self.assertEqual(parsed, t)

    def test_nested_tags(self):
        parsed = self.e.parse("[td]s[/td]", 0, self.subs, extra_tags=self.bb)
        t = "[td]s[/td]"
        self.assertEqual(parsed, t)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import builtins
import struct


class UnknownImageFormat(Exception):
    pass


class ImageTooLarge(Exception):
    pass


class ImageNotOpened(Exception):
    pass


class FileChecker:
    def __init__(self, max_size):
        self.max_size = max_size
        self.closed = True
        self.file = None
        self.file_type = None
        self.header = None
        self.width = None
        self.height = None
        self.size = None
        self.version = 0

    def open(self, file):
        if builtins.hasattr(file, "read") and builtins.callable(file.read):
            self.file = file
        else:
            self.file = open(file, "rb")  # pylint: disable=consider-using-with
            self.closed = False

        # First check to make sure we're not too large.
        self.file.seek(0, 2)
        self.size = self.file.tell()
        if self.size > self.max_size:
            raise ImageTooLarge("This image is too large to be checked.")
        self.file.seek(0)
        self.header = self.file.read(26)
        if len(self.header) != 26:
            raise UnknownImageFormat("Image was not identifiable as a PNG, JPG, GIF, or BMP.")
        if self.header[:6] in [b"GIF87a", b"GIF89a"]:
            self.file_type = "gif"
        elif self.header.startswith(b"\211PNG\r\n\032\n"):
            self.file_type = "png"
            if self.header[12:16] == b"IHDR":
                self.version = 1
        elif self.header.startswith(b"\377\330"):
            self.file_type = "jpeg"
        elif self.header.startswith(b"BM"):
            self.file_type = "bmp"
        else:
            raise UnknownImageFormat("Image was not identifiable as a PNG, JPG, GIF, or BMP.")

    def close(self):
        if not self.closed:
            self.file.close()
            self.closed = True

    def _get_metadata_bmp(self):
        headerlength = struct.unpack("<I", self.header[14:18])
        if headerlength == 12:
            self.width, self.height = struct.unpack("<2H", self.header[18:22])
        elif headerlength == 40:
            self.width, self.height = struct.unpack("<2i", self.header[18:26])
            self.height = -1 * self.height
        else:
            raise UnknownImageFormat("Image was not identifiable as a PNG, JPG, GIF, or BMP.")

    def _get_metadata_png(self):
        if self.version == 0:
            self.width, self.height = struct.unpack(">2L", self.header[8:16])
        elif self.version == 1:
            self.width, self.height = struct.unpack(">2L", self.header[16:24])

    def _get_metadata_gif(self):
        self.width, self.height = struct.unpack("<2H", self.header[6:10])

    def _get_metadata_jpeg(self):
        currentpos = self.file.tell()
        self.file.seek(2)
        b = self.file.read(1)
        count = 2
        try:
            while b and (ord(b) != 0xC0 and ord(b) != 0xC2) and count <= self.max_size:
                b = self.file.read(1)
                count += 1
            if ord(b) >= 0xC0 and ord(b) <= 0xC3:
                self.file.read(3)
                self.height, self.width = struct.unpack(">2H", self.file.read(4))
        except Exception as e:
            raise UnknownImageFormat("Image was not identifiable as a PNG, JPG, GIF, or BMP.") from e
        finally:
            self.file.seek(currentpos)

    def get_metadata(self):
        if not self.file_type or not self.header:
            raise ImageNotOpened("You must have an open image filehandle.")
        try:
            getattr(self, f"_get_metadata_{self.file_type}")()
        except AttributeError as e:
            raise UnknownImageFormat("Image was not identifiable as a PNG, JPG, GIF, or BMP.") from e
        return self.width, self.height

    def verify(self):
        # Think this looks bad? Well, it is. But it's the same thing Pillow does,
        # minus 25+ years of development.
        # https://pillow.readthedocs.io/en/stable/_modules/PIL/Image.html#Image.verify
        pass

    def __del__(self):
        if not self.closed:
            self.file.close()


if __name__ == "__main__":
    # Standard Library
    import sys

    f = FileChecker(100000000)
    f.open(sys.argv[1])
    print(f.file_type)
    print(f.get_metadata())
    f.verify()

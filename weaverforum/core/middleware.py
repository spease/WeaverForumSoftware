#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import secrets

# Weaver Libraries
from weaverforum import weaver_settings
from weaverforum.core.models import ForumConfiguration

# External Dependencies
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.db.models import Case, Value, When
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.deprecation import MiddlewareMixin

DISALLOWED_PATHS = [weaver_settings.MEDIA_URL, settings.STATIC_URL, settings.MEDIA_URL]


def get_currently_online(self):
    """Returns a list of (user, last action, last url), sorted by time, most recent to least"""
    user_ids = cache.get_many(self.currently_online_ids or [])
    ret_val = (
        User.objects.filter(id__in=self.currently_online_ids)
        .annotate(
            last_online=Case(
                *[
                    When(id=user_id, then=Value(user_ids[user_id][0].strftime("%Y%m%d%H%M%S")))
                    for user_id in user_ids
                ]
            ),
            time=Case(*[When(id=user_id, then=Value(user_ids[user_id][0])) for user_id in user_ids]),
            action=Case(*[When(id=user_id, then=Value(user_ids[user_id][1])) for user_id in user_ids]),
        )
        .select_related("profile")
        .only("profile__rank_id", "username", "pk")
        .order_by("-last_online")
    )
    return ret_val


class CSPMiddleware:
    """Adds CSP Headers to responses."""

    def __init__(self, get_response):
        self.policy = self.make_policy()

        self.header_name = "content-security-policy"
        if getattr(settings, "CSP_REPORT_ONLY", False):
            self.header_name += "-report-only"
        self.get_response = get_response

    def make_policy(self):
        report_uri = getattr(settings, "CSP_REPORT_TO", reverse_lazy("report_csp"))
        defaults = {
            "form-action": ["'self'"],
            "script-src": ["'self'", "'report-sample'"],
            "frame-src": ["www.youtube.com"],
            "style-src": ["'self'"],
            "font-src": ["'self'"],
            "default-src": ["'self'"],
            "img-src": ["'self'", "https://*", "http://*"],
            "connect-src": ["'self'"],
            "object-src": [],
            "worker-src": ["www.youtube.com"],
            "report-uri": report_uri,
            "report-to": "csp-endpoint",
        }

        policies = {}

        for directive, default_policy in defaults.items():
            settings_name = f"CSP_{directive.upper().replace('-', '_')}"
            policy = getattr(settings, settings_name, default_policy)
            if len(policy) > 0:
                if isinstance(policy, list):
                    policy = " ".join(policy)
                policies[directive] = policy

        return policies

    def get_policy(self, nonce):
        policies = []

        for directive, sources in self.policy.items():
            if directive in ["style-src", "script-src"]:
                sources += f" 'nonce-{nonce}'"
            policies.append(f"{directive} {sources}")

        return "; ".join(policies)

    def __call__(self, request):
        nonce = secrets.token_urlsafe()
        # Inspector won't actually show the nonce, if you're wondering why it's broken- it's not.
        request.csp_nonce = nonce
        response = self.get_response(request)

        response[self.header_name] = self.get_policy(nonce)
        report_uri = getattr(settings, "CSP_REPORT_TO", reverse_lazy("report_csp"))
        response["report-to"] = {
            "group": "csp-endpoint",
            "max_age": 10886400,
            "endpoints": [{"url": report_uri}],
        }
        response["report-uri"] = response["report-to"]
        return response


class CurrentlyOnlineMiddleware(MiddlewareMixin):  # pylint: disable=too-few-public-methods
    def process_request(self, request):
        if "report-csp-violation" in request.path_info:  # pragma: no cover
            return

        fc = ForumConfiguration.load()
        stale_ids = cache.get("currently-online", [])

        # Turn ids to keys, find which are still valid, and return the valid list to ids.
        stale_currently_online_keys = [f"{user_id}" for user_id in stale_ids]
        valid_currently_online_keys = cache.get_many(stale_currently_online_keys).keys()
        currently_online_ids = [int(key) for key in valid_currently_online_keys]

        uid = None
        if request.user.is_authenticated:
            uid = request.user.id
            if uid in currently_online_ids:
                currently_online_ids.remove(uid)
            currently_online_ids.append(uid)
            currently_online_ids = currently_online_ids[(-1 * fc.currently_online_max) :]

        # place in request so we can reach them in templates
        request.__class__.currently_online_ids = currently_online_ids
        request.__class__.currently_online = property(get_currently_online)

        disallowed_path = any(request.get_full_path().startswith(path) for path in DISALLOWED_PATHS)

        # Set the new cache
        if uid and not disallowed_path:
            cache.set_many(
                {
                    uid: (timezone.now(), request.get_full_path()),
                    "currently-online": currently_online_ids,
                },
                fc.last_online_timeout,
            )
        else:
            cache.set("currently-online", currently_online_ids, fc.last_online_timeout)

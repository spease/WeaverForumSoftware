# Standard Library
import os
import re
import uuid
import zoneinfo

# Weaver Libraries
from weaverforum import weaver_settings
from weaverforum.core.embbroider.tags import ez_tag

# External Dependencies
from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.db.utils import IntegrityError, OperationalError
from django.utils.deconstruct import deconstructible

TIMEZONES = [
    (zone, zone) for zone in zoneinfo.available_timezones() if (not zone.startswith("Etc") and "/" in zone)
]
TIMEZONES.sort(key=lambda x: x[0])

# With thanks to Eric Pruitt at https://stackoverflow.com/a/50735610
JS_CSS_COMMENT_STRIPPING_REGEX = re.compile(
    r"""
    # Quoted strings
    ( "(?:[^"\\]+|\\.)*" | '(?:[^'\\]+|\\.)*' )
    |
    # Comments
    /\* ( .*? \*/ )
    """,
    re.DOTALL | re.VERBOSE,
)

JS_CSS_MINI_REGEX = re.compile(
    r"""
    # Quoted strings
    ( "(?:[^"\\]+|\\.)*" | '(?:[^'\\]+|\\.)*' )
    |
    # Spaces before and after ";" and "}"
    \s* ; \s* ( } ) \s*
    |
    # Spaces around meta characters and operators excluding "+" and "-"
    \s* ( [*$~^|]?= | [{};,>~] | !important\b ) \s*
    |
    # Spaces around "+" and "-" in selectors only
    \s*([+-])\s*(?=[^}]*{)
    |
    # Spaces to the right of "(", "[" and ":"
    ( [\[(:] ) \s+
    |
    # Spaces to the left of ")" and "]"
    \s+ ( [\])] )
    |
    # Spaces around ":" outside of selectors
    \s+(:)(?![^\}]*\{)
    |
    # Spaces at the beginning and end of the string
    ^ \s+ | \s+ \Z
    |
    # Collapse concurrent spaces
    (\s)\s+
    """,
    re.DOTALL | re.IGNORECASE | re.VERBOSE,
)


@deconstructible
class IconUpload:  # pylint: disable=too-few-public-methods
    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        return os.path.join(self.sub_path, "favicon.gif")


class Theme(models.Model):
    id = models.BigAutoField(primary_key=True)
    dir = weaver_settings.MEDIA_ROOT
    name = models.CharField(max_length=156, blank=False, null=False)
    active = models.BooleanField(default=True)
    css = models.TextField(blank=False, null=False)
    filename = models.CharField(max_length=256)

    def __str__(self):
        return ("{}" if self.active else "[Disabled] {}").format(self.name)

    def save(self, *args, **kwargs):
        if not self.pk or self.pk not in [1, 2]:
            old_filename = self.filename
            unique = uuid.uuid4().hex
            new_filename = f"{unique}.css"
            self.filename = new_filename
            os.makedirs(self.dir, exist_ok=True)
            with open(self.dir + new_filename, "w", encoding="utf-8") as file:
                file.write(Theme.minify(self.css))
        super().save(*args, **kwargs)
        if self.pk not in [1, 2]:
            if os.path.exists(self.dir + old_filename) and old_filename:
                os.remove(self.dir + old_filename)

    def delete(self, *args, **kwargs):
        if self.pk in [1, 2]:
            return
        try:
            os.remove(self.dir + self.filename)
        except OSError:
            pass
        super().delete(*args, **kwargs)

    def get_filename(self):
        if self.pk == 1:
            return settings.STATIC_URL + "weaver.css"
        if self.pk == 2:
            return settings.STATIC_URL + "weaver-dark.css"
        return weaver_settings.MEDIA_URL + self.filename

    @classmethod
    def minify(cls, text):
        return JS_CSS_MINI_REGEX.sub(r"\1\2\3\4\5\6\7\8", JS_CSS_COMMENT_STRIPPING_REGEX.sub(r"\1", text))


class ForumConfiguration(models.Model):
    id = models.BigAutoField(primary_key=True)
    forum_name = models.CharField(
        max_length=255,
        default="Weaver Forums",
        blank=False,
        null=False,
        help_text="The name of your forum. Default: 'Weaver Forums'",
    )
    forum_banner = models.TextField(
        blank=False,
        null=False,
        default="<b>WEAVER</b>FORUMS",
        help_text=("The banner for your forum. HTML is allowed. Default: " "'<b>WEAVER</b>FORUMS'"),
    )
    forum_icon = models.FileField(upload_to=IconUpload(""), blank=True)
    forum_announcement = models.TextField(
        blank=True,
        null=False,
        default="",
        help_text=("The announcement container for your forum. Default:" "''"),
    )
    last_online_timeout = models.IntegerField(
        blank=False,
        null=False,
        default=60 * 15,
        help_text=(
            "How long (in seconds) a user is considered logged"
            " on without loading a new page. Default: '900'"
        ),
    )
    last_online_expiry = models.IntegerField(
        blank=False,
        null=False,
        default=60 * 60 * 24 * 7,
        help_text=("How long (in seconds) to keep track of last online " "state. Default: '604800'"),
    )
    currently_online_max = models.IntegerField(
        blank=False,
        null=False,
        default=150,
        help_text=(
            "Maximum number of users online to keep track of. "
            "Raising this requires more RAM resources. "
            "Default: '150'"
        ),
    )
    allow_guest_read = models.BooleanField(
        default=False,
        help_text=("Are non-registered guests allowed to" " read the forum? Default: False"),
    )
    new_user_approval = models.BooleanField(
        default=True,
        help_text=("Do new users require admin approval?" " Default: True"),
    )
    default_timezone = models.CharField(
        default="US/Eastern",
        blank=False,
        null=False,
        max_length=32,
        help_text="The default timezone of the board. Default: 'US/Eastern'",
    )
    default_theme = models.ForeignKey(
        "Theme",
        on_delete=models.PROTECT,
        limit_choices_to={"active": True},
        default=1,
        help_text=("The default theme of the board. " "Default: 'Default Light'"),
        blank=False,
        null=False,
    )
    avatar_max_size = models.IntegerField(
        blank=False,
        null=False,
        default=10,
        help_text=(
            "The max size of a user's avatar in kb. A zero value "
            "here or in Avatar Max Dimensions turns off avatars. "
            "Default: '10'"
        ),
    )
    avatar_max_dimensions = models.IntegerField(
        blank=False,
        null=False,
        default=64,
        help_text=("The max width/height of a user's avatar in " "pixels. Default: '64'"),
    )

    def __repr__(self):
        return "Forum Configuration"

    def __str__(self):
        return "Forum Configuration"

    class Meta:  # pylint: disable=too-few-public-methods
        verbose_name = "Forum Configuration"
        verbose_name_plural = "Forum Configuration"

    def save(self, *args, **kwargs):
        self.pk = 1
        super().save(*args, **kwargs)
        cache.set("forum_settings", self, None)

    def delete(self, *args, **kwargs):
        pass  # pragma: no cover

    @classmethod
    def load(cls):
        # Note that this design can technically have a race condition in c being incorrect.
        # As of this time, that race condition does not matter, but be aware of it.
        c = cache.get("forum_settings")
        if c is None:
            try:
                obj, created = cls.objects.get_or_create(pk=1)
                if not created:
                    c = obj
                    cache.set("forum_settings", obj, None)
            except (
                OperationalError,
                IntegrityError,
            ):  # Initial migrate: no table  # pragma: no cover
                pass
        return c


class BBcodeTag(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(
        max_length=64,
        blank=False,
        null=False,
        help_text="The text to be enclosed in brackets to call this tag.",
    )
    param_regex = models.CharField(
        max_length=64,
        blank=True,
        null=False,
        help_text=("A valid regex that a bbcode parameter must meet for this tag " "to be valid."),
    )
    html = models.TextField(
        blank=False,
        null=False,
        help_text=(
            "The HTML that will replace the BBcode. "
            "You may use {content} and {param} to fill "
            "out the template. Use {{foo}} to get the "
            "actual text {foo}."
        ),
    )
    is_solo_tag = models.BooleanField(default=False, help_text="Should this have a closing tag?")
    strip_outer = models.BooleanField(default=False, help_text="Strip newlines after the tag?")
    strip_inner = models.BooleanField(default=False, help_text="Strip newlines/whitespace inside the tag?")
    discard_text = models.BooleanField(
        default=False, help_text=("Should we discard any plaintext within " "this tag?")
    )
    parse_contents = models.BooleanField(
        default=True,
        help_text=("Should we parse BBcode within the contents" " of this tag?"),
    )
    do_substitutions = models.BooleanField(
        default=True,
        help_text=("Should we execute non-permanent " "substitutions in this tag?"),
    )

    class Meta:  # pylint: disable=too-few-public-methods
        verbose_name = "BBcode"

    def __str__(self):
        return self.name

    @staticmethod
    def _make_cache():
        tags_cache = {}
        for tag in BBcodeTag.objects.all():
            tags_cache[tag.name] = ez_tag(
                tag.name,
                IS_SOLO_TAG=tag.is_solo_tag,
                PARAM_REGEX=tag.param_regex,
                HTML=tag.html,
                STRIP_OUTER=tag.strip_outer,
                STRIP_INNER=tag.strip_inner,
                DISCARD_TEXT=tag.discard_text,
                PARSE_CONTENTS=tag.parse_contents,
                DO_SUBSTITUTIONS=tag.do_substitutions,
            )
        return tags_cache

    @classmethod
    def load(cls):
        # Note that this design can technically have a race condition in c being incorrect.
        # As of this time, that race condition does not matter, but be aware of it.
        t = cache.get("tags")
        if t is None:
            t = cls._make_cache()
            cache.set("tags", t, None)
        return t

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        super().save(*args, **kwargs)
        cache.set("tags", self._make_cache())


class Substitution(models.Model):
    id = models.BigAutoField(primary_key=True)
    match = models.CharField(max_length=256, blank=False, null=False)
    replace = models.CharField(max_length=256, blank=False, null=False)
    permanent = models.BooleanField(
        default=True,
        help_text=(
            "Will this replacement permanently change "
            "a user's text? For example, something like "
            "© should be non-permanent, but a curse word "
            "filter should be permanent. Non-permanent "
            "filters can have HTML in them."
        ),
    )

    def __str__(self):
        perm = "Permanent" if self.permanent else "Display"
        return f"[{perm}] {self.match}->{self.replace}"

    @staticmethod
    def _make_cache():
        scache = {
            "PD": {},
            "ND": {
                "(tm)": "™",
                "(c)": "©",
                "(reg)": "®",
                # Emoji defaults
                ":)": "🙂",
                ":D": "😀",
                ":joy:": "😂",
                "xD": "😆",
                " ;)": " 😉",
                "B)": "😎",
                ":|": "😐",
                "-_-": "😑",
                ">.>": "😒",
                ">_>": "😒",
                ":P": "😛",
                ":(": "🙁",
                ":o": "😮",
                "T_T": "😭",
                ":E": "😬",
                ">_<": "😣",
                ">.<": "😣",
                "o_o": "😲",
                "0_0": "😲",
                "o.o": "😲",
                "0.0": "😲",
                "@_@": "😵",
                ":rolleyes:": "🙄",
                ":clap:": "🙌",
            },
        }
        for sub in Substitution.objects.all():
            perm = "PD" if sub.permanent else "ND"
            scache[perm][sub.match] = sub.replace
        scache["P"] = re.compile("|".join(map(re.escape, sorted(scache["PD"], key=len, reverse=True))), re.I)
        scache["N"] = re.compile("|".join(map(re.escape, sorted(scache["ND"], key=len, reverse=True))))
        return scache

    @classmethod
    def load(cls):
        # Note that this design can technically have a race condition in c being incorrect.
        # As of this time, that race condition does not matter, but be aware of it.
        s = cache.get("subs")
        if s is None:
            s = cls._make_cache()
            cache.set("subs", s)
        return s

    def save(self, *args, **kwargs):
        self.match = self.match.lower()

        super().save(*args, **kwargs)
        cache.set("subs", self._make_cache(), None)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import re
from html import escape

# Weaver Libraries
from weaverforum.core.embbroider.tags import DEFAULT_TAGS, Tag

TOKENIZER_RE = re.compile(r"((?<!\\)\[\/?.+?\])")
TOKENNAME_RE = re.compile(r"\[(?P<end>\/?)(?P<tag_name>[\*a-zA-Z0-9_-]+?)(?:=(?P<tag_option>[^\]]+))?\]")


class emBBroider:  # pylint: disable=invalid-name, too-few-public-methods
    def __init__(self):
        self.tags = {}

    def _close_tag(self, current, token, token_type):
        lookbehind = current
        while lookbehind is not None:
            if lookbehind.name == token_type and lookbehind.render_text:
                lookbehind.render_text = False
                return current.parent
            lookbehind = lookbehind.parent
        return self._create_text_node(token, current)

    def _create_tag(self, token_type, token, parent, param):
        cls = self.tags.get(token_type.lower())
        if cls and cls.PARAM_REGEX:
            if not param or not re.match(cls.PARAM_REGEX, param):
                cls = None
        else:
            if param:
                param = escape(param)
        if cls and cls.PARENT_TAG and parent and (not parent.name or parent.name not in cls.PARENT_TAG):
            cls = None
        if cls:
            new_tag = cls(post_id=None, name=token_type, parent=parent, param=param, text="")
            if new_tag.IS_SOLO_TAG:
                return new_tag.parent
            return new_tag
        return self._create_text_node(token, parent)

    def _create_text_node(self, token, parent):
        if parent and parent.children and parent.children[-1].STRIP_OUTER:
            token = token.lstrip()
        if parent.STRIP_INNER:
            token = token.strip()
        if parent.DISCARD_TEXT or token == "":  # nosec B105, this is not a hardcoded password.
            return parent
        if parent.children and parent.children[-1].name == "TEXT":
            parent.children[-1].text += token
        else:
            Tag("TEXT", parent=parent, text=token, post_id=0, param="")
        return parent

    def parse(self, obj, post_id, subs, extra_tags=None):
        """
        Public function. Tokenizes and parses BBcode to HTML.
        """
        tokens = TOKENIZER_RE.split(obj)
        cursor = 0
        current = root = Tag(post_id=post_id, name=None, param="", text="", parent=None)
        self.tags = DEFAULT_TAGS
        for tag in extra_tags:
            self.tags[tag] = extra_tags[tag]

        while cursor < len(tokens):
            token = tokens[cursor]
            match = re.match(TOKENNAME_RE, token)
            if match:
                token_type = match.group("tag_name")
                token_option = match.group("tag_option")
                token_end = match.group("end")
                if not token_end:
                    if current.PARSE_CONTENTS:
                        current = self._create_tag(token_type, token, current, token_option)
                    else:
                        current = self._create_text_node(token, current)
                else:
                    current = self._close_tag(current, token, token_type)
            else:
                current = self._create_text_node(token, current)
            cursor += 1
        return root.to_html(subs)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# pylint: disable=too-few-public-methods

# Standard Library
import re
from dataclasses import dataclass, field
from html import escape

LINKIFY_RE = re.compile(
    r"(https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&\/=]*)"
)


@dataclass
class Tag:
    name: str
    post_id: int = field(repr=False)
    parent: "Tag" = field(repr=False)
    text: str = field(repr=False)
    param: str = field(repr=False)

    render_text = True
    bb_counter = 0

    IS_SOLO_TAG = False
    PARENT_TAG = []
    PARAM_REGEX = None
    HTML = "{content}"
    STRIP_OUTER = False
    STRIP_INNER = False
    DISCARD_TEXT = False
    PARSE_CONTENTS = True
    DO_SUBSTITUTIONS = True  # pylint: disable=invalid-name

    def __post_init__(self):
        self.root = self
        if self.parent:
            self.root = self.parent.root
            self.parent.children.append(self)
            if not self.parent.DO_SUBSTITUTIONS:
                self.DO_SUBSTITUTIONS = False  # pylint: disable=invalid-name
        if self.IS_SOLO_TAG:
            self.render_text = False
        self.children = []
        self.toc_children = []

    def to_html(self, subs, **kwargs):
        content = "".join([str(x.to_html(subs)) for x in self.children])
        text = escape(self.text).replace("\r\n", "\n").replace("\n", "<br />\n").replace(r"\[", "[")
        if self.DO_SUBSTITUTIONS:
            text = LINKIFY_RE.sub(r'<a href="\1" target="_blank" rel="noreferrer">\1</a>', text)
            text = subs["N"].sub(lambda match: subs["ND"][match.group(0)], text)
        try:
            if self.render_text and (self.name != "TEXT" and self.name is not None):
                if self.param and str(self.param):
                    param = f"={self.param}"
                else:
                    param = ""
                return f"[{self.name}{param}]{content}"
            content += text
            return self.HTML.format(content=content, param=self.param, **kwargs)
        # We want to do a general catchall here, in case of some crazy BS custom tag.
        except:  # pragma: no cover  # pylint: disable=bare-except
            return "[Error in Admin-Set Tag]"

    def __str__(self):
        return self.name


class TocTag(Tag):
    IS_SOLO_TAG = True
    STRIP_OUTER = True

    def to_html(self, subs, **kwargs):
        toc_header = "<b>Table of Contents</b><br />"
        toc_string = "<br>".join(
            [
                f'<span class="bb_toc_link"><a href="{x.param_id}">{x.get_content(subs)}</a></span>'
                for x in self.root.toc_children
            ]
        )
        return f'<div class="bb_toc">{toc_header}{toc_string}</div><br /><br />'


class TocLinkTag(Tag):
    PARAM_REGEX = "^https://|^http://"
    HTML = ""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.root:
            self.param_id = self.param
            self.root.toc_children.append(self)

    def get_content(self, subs):
        return "".join([x.to_html(subs) for x in self.children])


class ListTag(Tag):
    STRIP_OUTER = True
    STRIP_INNER = True
    DISCARD_TEXT = True
    HTML = "<ul>{content}</ul>"


class ListItemTag(Tag):
    PARENT_TAG = ("list",)
    HTML = "<li>{content}</li>"


class RightTag(Tag):
    HTML = '<div class="bb_right">{content}</div>'


class QuoteTag(Tag):
    STRIP_INNER = True
    STRIP_OUTER = True

    def to_html(self, subs, **kwargs):
        if self.param:
            self.HTML = "<blockquote>{content}<br />-{param}</blockquote>"  # pylint: disable=invalid-name
        else:
            self.HTML = "<blockquote>{content}</blockquote>"  # pylint: disable=invalid-name
        return super().to_html(subs)


class ImgTag(Tag):
    STRIP_INNER = True
    PARSE_CONTENTS = False
    DO_SUBSTITUTIONS = False
    HTML = '<img src="{content}" alt="{param}"/>'


class UrlTag(Tag):
    HTML = '<a href="{param}" target="_blank" rel="noreferrer">{content}</a>'
    PARAM_REGEX = "^https://|^http://"


class SpoilerTag(Tag):
    HTML = '<details><summary class="spoiler">Spoiler: {param}</summary>{content}</details>'
    PARAM_REGEX = r"^[^\[\]]+$"

    def to_html(self, subs, **kwargs):
        param_id = f"{self.root.post_id}_{self.root.bb_counter}"
        self.root.bb_counter += 1
        return super().to_html(subs, param_id=param_id)


class ToggleTag(Tag):
    HTML = "<details><summary>{param}</summary>{content}</details>"
    PARAM_REGEX = r"^[^\[\]]+$"

    def to_html(self, subs, **kwargs):
        param_id = f"{self.root.post_id}_{self.root.bb_counter}"
        self.root.bb_counter += 1
        return super().to_html(subs, param_id=param_id)


class AnchorTag(Tag):
    HTML = '<a href="#a_{param}" id="a_{param}" class="bb_anchor">{content}</a>'
    PARAM_REGEX = r"^[a-zA-Z0-9\-]+$"

    def get_content(self, subs):
        return "".join([x.to_html(subs) for x in self.children])


class TocAnchorTag(Tag):
    HTML = '<a href="#a_{param}" id="a_{param}" class="bb_tocanchor">{content}</a>'
    PARAM_REGEX = r"^[a-zA-Z0-9\-]+$"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.root:
            self.param_id = f"#a_{escape(self.param)}"
            self.root.toc_children.append(self)

    def get_content(self, subs):
        return "".join([x.to_html(subs) for x in self.children])


class TabsTag(Tag):
    STRIP_OUTER = True
    STRIP_INNER = True
    DISCARD_TEXT = True

    def to_html(self, subs, **kwargs):
        full_tabs = []
        is_first = " checked"
        for child in self.children:
            param_id = f"{self.root.post_id}_{self.root.bb_counter}"

            self.root.bb_counter += 1
            full_tabs.append(
                (
                    f'<input class="tabinput" id="bb_tabtag_{param_id}" type="radio" name="tabs" '
                    f'{is_first}>\n<label class="tablabel" for="bb_tabtag_{param_id}">{child.param}'
                    f"</label>{child.to_html(subs)}"
                )
            )
            is_first = ""
        full_tabs = "\n".join(full_tabs)
        return f'<br /><div class="bb_tab">{full_tabs}</div><br />'


class TabTag(Tag):
    PARAM_REGEX = r"^[\w \.\'\"]+$"
    HTML = '<div class="tabpanel"><p>{content}</p></div>'


class TooltipTag(Tag):
    HTML = '<b title="{param}">{content}</b>'


class YoutubeTag(Tag):
    STRIP_INNER = True
    IS_SOLO_TAG = True
    PARSE_CONTENTS = False
    DO_SUBSTITUTIONS = False
    HTML = (
        '<iframe width="324" height="240" src="https://www.youtube.com/embed/{param}" '
        'frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'
    )
    PARAM_REGEX = r"^([a-zA-Z0-9\-\_]{11,15})$"


class SizeTag(Tag):
    HTML = '<span class="bb-size" bb-size="{param}px">{content}</span>'
    PARAM_REGEX = r"^\d+$"


class ColorTag(Tag):
    HTML = '<span class="bb-color" bb-color="{param}">{content}</span>'
    # 20 is the length of the longest built-in CSS code, LightYellowGoldenRod.
    PARAM_REGEX = "^(?:#[0-9a-fA-F]{3}|#[0-9a-fA-F]{6}|[a-zA-Z]{0,20})$"


def ez_tag(name, **attributes):
    if "IS_SOLO_TAG" in attributes and attributes["IS_SOLO_TAG"]:
        attributes["HTML"] = f"<{name} />"
    else:
        attributes["HTML"] = f"<{name}>{{content}}</{name}>"
    tag_name = f"{name.title()}Tag"
    tag_object = type(tag_name, (Tag,), attributes)
    globals()[tag_name] = tag_object
    return tag_object


DEFAULT_TAGS = {
    "toc": TocTag,
    "toclink": TocLinkTag,
    "hr": ez_tag("hr", IS_SOLO_TAG=True, STRIP_OUTER=True),
    "list": ListTag,
    "li": ListItemTag,
    "h1": ez_tag("h1", STRIP_OUTER=True),
    "h2": ez_tag("h2", STRIP_OUTER=True),
    "h3": ez_tag("h3", STRIP_OUTER=True),
    "h4": ez_tag("h4", STRIP_OUTER=True),
    "h5": ez_tag("h5", STRIP_OUTER=True),
    "h6": ez_tag("h6", STRIP_OUTER=True),
    "color": ColorTag,
    "pre": ez_tag("pre", STRIP_OUTER=True),
    "code": ez_tag("code", PARSE_CONTENTS=False, DO_SUBSTITUTIONS=False),
    "table": ez_tag("table", DISCARD_TEXT=True),
    "thead": ez_tag("thead", PARENT_TAG=("table",), DISCARD_TEXT=True),
    "tbody": ez_tag("tbody", PARENT_TAG=("table",), DISCARD_TEXT=True),
    "tr": ez_tag("tr", PARENT_TAG=("tbody", "thead"), DISCARD_TEXT=True),
    "th": ez_tag("th", PARENT_TAG=("tr",), STRIP_INNER=True),
    "td": ez_tag("td", PARENT_TAG=("tr"), STRIP_INNER=True),
    "sup": ez_tag("sup"),
    "sub": ez_tag("sub"),
    "b": ez_tag("b"),
    "u": ez_tag("u"),
    "i": ez_tag("em"),
    "s": ez_tag("strike"),
    "center": ez_tag("center"),
    "right": RightTag,
    "quote": QuoteTag,
    "img": ImgTag,
    "url": UrlTag,
    "spoiler": SpoilerTag,
    "toggle": ToggleTag,
    "anchor": AnchorTag,
    "tocanchor": TocAnchorTag,
    "tabs": TabsTag,
    "tab": TabTag,
    "tooltip": TooltipTag,
    "youtube": YoutubeTag,
    "size": SizeTag,
    "TEXT": Tag,
}

# Standard Library
import json
import logging

# External Dependencies
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

logger = logging.getLogger("csp-logger")


@require_POST
@csrf_exempt
def report_csp(request):  # pragma: no cover
    raw_report = request.body
    if isinstance(raw_report, bytes):
        raw_report = raw_report.decode("utf-8")
    try:
        report = json.loads(raw_report)
        try:
            report["content_type"] = request.content_type
            report["content_params"] = request.content_params
            report["method"] = request.method
            report["agent"] = request.META["HTTP_USER_AGENT"]
        except IndexError:
            pass
        report = json.dumps(report, sort_keys=True, indent=4)
    except ValueError:
        report = (
            "Error parsing JSON. Raws:\n",
            "---------------------------------------------------------------------------------",
            f"\n{raw_report}",
        )
    logger.warning(report)
    return HttpResponse("")

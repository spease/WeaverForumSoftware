#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum import weaver_settings
from weaverforum.profiles.models import PrivateMessage

# External Dependencies
from django.contrib.auth import get_user_model
from django.core.mail import send_mail as django_send_mail


def send_mail(  # pylint: disable=too-many-arguments
    subject,
    message,
    recipient_list,
    from_email=weaver_settings.FROM_EMAIL,
    fail_silently=False,
    auth_user=None,
    auth_password=None,
    connection=None,
    html_message=None,
):
    try:
        return django_send_mail(
            subject,
            message,
            from_email,
            recipient_list,
            fail_silently,
            auth_user,
            auth_password,
            connection,
            html_message,
        )
    except ConnectionRefusedError:
        for admin in get_user_model.objects.filter(is_staff=True):
            PrivateMessage.objects.create(
                system_message=True,
                created_by=None,
                sent_to=admin,
                text=(
                    "There is likely a problem sending emails, and you should contact your system "
                    "administrator.\n\n"
                    f"Subject: {subject}\n\n"
                    f"Recipients: {', '.join(recipient_list)}\n\n"
                    f"Email body:\n{message}"
                ),
                title="System Error: Unable To Send Email",
            )
        return 0

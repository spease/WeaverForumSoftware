# Weaver Libraries
from weaverforum.core.models import BBcodeTag, ForumConfiguration, Substitution, Theme
from weaverforum.forums.models import Rank

# External Dependencies
from django import forms
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.urls import re_path

User = get_user_model()


class ForumConfigurationAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_urls(self):
        urls = super().get_urls()
        hijack_urls = [
            re_path(
                r"^history/$",
                self.admin_site.admin_view(self.history_view),
                {"object_id": "1"},
                name="cores_forumconfigurations_history",
            ),
            re_path(
                r"^$",
                self.admin_site.admin_view(self.change_view),
                {"object_id": "1"},
                name="cores_forumconfigurations_change",
            ),
        ]
        return hijack_urls + urls

    def response_change(self, request, obj):
        msg = "Successfully changed settings."
        if "_continue" in request.POST:
            self.message_user(request, msg + " " + "You may edit them again below.")
            return HttpResponseRedirect(request.path)
        self.message_user(request, msg)
        return HttpResponseRedirect("../../")

    def change_view(self, request, object_id, form_url="", extra_context=None):
        if object_id == "1":
            self.model.objects.get_or_create(pk=1)
        return super().change_view(request, object_id, form_url=form_url, extra_context=extra_context)


class RankAdmin(admin.ModelAdmin):  # pylint: disable=too-few-public-methods
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "display_title",
                    "order_val",
                    "type",
                    "post_count",
                    "style",
                    "is_admin",
                )
            },
        ),
        (
            "Default Permissions",
            {
                "fields": (
                    (  # pylint: disable=duplicate-code
                        "can_view",
                        "can_post",
                        "can_edit_own",
                        "can_edit_other",
                        "can_delete_own",
                        "can_delete_other",
                        "can_view_deleted",
                        "can_lock",
                        "can_post_locked",
                        "can_pin",
                        "can_mod",
                        "can_ban",
                    ),
                )
            },
        ),
    )

    def has_delete_permission(self, request, obj=None):
        # no deleting the guest or default, no deleting the last admin
        if not obj:
            return False
        if obj.type in ["D", "G"] or (obj.is_admin and type(obj).objects.filter(is_admin=True).count() == 1):
            return False
        return True


class ThemeAdmin(admin.ModelAdmin):
    fieldsets = [[None, {"fields": ["name", "active"]}]]

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        if not obj or obj.pk not in [1, 2]:
            if "css" not in fieldsets[0][1]["fields"]:
                fieldsets[0][1]["fields"].append("css")
        else:
            if "css" in fieldsets[0][1]["fields"]:
                fieldsets[0][1]["fields"].remove("css")
        return fieldsets

    def clean(self):
        if not self.cleaned_data["active"] and Theme.objects.filter(active=True).count() <= 1:
            raise forms.ValidationError("You must have at least one active theme.")

    def has_delete_permission(self, request, obj=None):
        # Do not allow delete for the two built-in themes.
        fc = ForumConfiguration.load()
        if obj and obj == fc.default_theme:
            return False
        return super().has_delete_permission(request, obj) and obj and obj.pk != 1 and obj.pk != 2


admin.site.register(ForumConfiguration, ForumConfigurationAdmin)
admin.site.unregister(Group)
admin.site.register(Rank, RankAdmin)
admin.site.register(BBcodeTag)
admin.site.register(Substitution)
admin.site.register(Theme, ThemeAdmin)

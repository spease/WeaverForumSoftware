# Standard Library
import importlib
import os
import traceback

# Weaver Libraries
from weaverforum import weaver_settings

# External Dependencies
from django.apps import AppConfig
from django.conf import settings
from django.db.backends.signals import connection_created
from django.dispatch import receiver


class WeaverForumPluginsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "weaverforum.load_plugins"
    verbose_name = "WeaverForum Plugins"

    def ready(self):
        disallowed_plugin_names = ["__pycache__", "__init__.py", "migrations"]

        files = []
        if not os.path.isdir(weaver_settings.PLUGIN_DIR):
            print("No plugin folder found.")
            return
        for file in os.listdir(weaver_settings.PLUGIN_DIR):
            if file not in disallowed_plugin_names and not file.startswith("."):
                plugin_module_name = f"{weaver_settings.PLUGIN_MODULE}.{os.path.splitext(file)[0]}"
                files.append(plugin_module_name)

        if sorted(files) != sorted(list(set(files))):  # if there are dupes/conflicts
            print("Multiple plugins have the same name. Fatal error.")
            print(files)
            print(list(set(files)))
            raise SystemExit(-1)

        for file in files:
            try:
                if importlib.util.find_spec(file):
                    importlib.import_module(file)
            except Exception as e:
                traceback.print_exc()
                print(f"ERROR: Unable to import plugin {file}.")
                raise SystemExit(-1) from e


@receiver(connection_created)
def load_plugins(*args, **kwargs):
    if getattr(settings, "WEAVERFORUM_PLUGINS_LOADED", False):
        return
    try:
        # Weaver Libraries
        from weaverforum.plugins.models import Plugin  # pylint: disable=import-outside-toplevel

        # we have to do the import here, it's simply what django requires
        if not Plugin.load_models():
            print("Aborting plugin load, unable to load plugin models.")
            return
        plugin_list = [x.__module__.split(".")[-1] for _, x in Plugin.get_plugins(include_inactive=True)]
        print(f"{len(plugin_list)} Plugins Loaded: " + ",".join(plugin_list))
        settings.WEAVERFORUM_PLUGINS_LOADED = True
    except Exception as e:
        traceback.print_exc()
        print("ERROR: Unable to load plugins.")
        raise SystemExit(-1) from e

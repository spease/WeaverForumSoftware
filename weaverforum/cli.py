#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import argparse
import ast
import contextlib
import logging
import os
import re
import shutil
import sys
from collections.abc import Iterable

# Weaver Libraries
from weaverforum.version import VERSION

# External Dependencies
from django.core.management import call_command

LOG = logging.getLogger(sys.argv[0])
LOG.setLevel(logging.WARNING)
LOG.addHandler(logging.StreamHandler())


@contextlib.contextmanager
def mod_file(filename):
    handler = None
    try:
        with open(filename, "r", encoding="utf-8") as f:
            handler = ModifyFile(f.read(), filename)
            yield handler
    except Exception as e:
        raise e
    finally:
        if handler:
            with open(filename, "w", encoding="utf-8") as f:
                f.write(handler.get_contents())


class Visitor(ast.NodeVisitor):
    def __init__(self, selector):
        # TEMPLATES[0]['DIR'] becomes ['TEMPLATES', '0' "'DIRS'"]
        self.selector = [s for s in re.split(r"[\.\[\]]", selector) if s]
        self.pos = 0
        self.found = None

    def visit(self, node):
        """Visit a node. Mostly a copy of the standard Node Visitor .visit(),
        but we also check if we've found our node here."""
        method = "visit_" + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        visitor(node)

    def visit_Assign(self, node):  # pylint: disable=invalid-name
        s_type, s_val = self._get_node_type(self.selector[self.pos])
        s_type, s_val = self._get_node_type(self.selector[self.pos])
        if s_type == "var" and s_val in [name.id for name in node.targets]:
            self.pos += 1
            node.value.parent = node
            if self.pos == len(self.selector):
                self.found = node.value
            else:
                self.visit(node.value)
        else:
            self.pos = 0
            self.generic_visit(node)

    def visit_Dict(self, node):  # pylint: disable=invalid-name
        s_type, s_val = self._get_node_type(self.selector[self.pos])
        s_type, s_val = self._get_node_type(self.selector[self.pos])
        keys = [key.value for key in node.keys]
        if s_type == "key" and s_val in keys:
            index = keys.index(s_val)
            self.pos += 1
            node.values[index].parent = node
            if self.pos == len(self.selector):
                self.found = node.values[index]
            else:
                self.visit(node.values[index])
        else:
            self.pos = 0
            self.generic_visit(node)

    def visit_List(self, node):  # pylint: disable=invalid-name
        s_type, s_val = self._get_node_type(self.selector[self.pos])
        s_type, s_val = self._get_node_type(self.selector[self.pos])
        if s_type == "index" and s_val < len(node.elts):
            self.pos += 1
            node.elts[s_val].parent = node
            if self.pos == len(self.selector):
                self.found = node.elts[s_val]
            else:
                self.visit(node.elts[s_val])
        else:
            self.pos = 0
            self.generic_visit(node)

    def generic_visit(self, node):
        for _field, value in ast.iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if self.found:
                        return
                    if isinstance(item, ast.AST):
                        item.parent = node
                        self.visit(item)
            elif isinstance(value, ast.AST):
                value.parent = node
                self.visit(value)

    def _get_node_type(self, chunk):
        if chunk[0] == chunk[-1] and (chunk[0] in ["'", '"']):
            return "key", chunk[1:-1]
        try:
            int(chunk)
            return "index", int(chunk)
        except:  # pylint: disable=bare-except
            return "var", chunk


class ModifyFile:
    """This is ineffecient because we're re-doing the AST and modifying the entire file in memory each time
    for every change, but this way we can stay up-to-date on any changes without worrying about
    managing line number offsets changing.
    Not to mention, weaveradmin really doesn't get ran all that often, and if you have a larger settings.py
    or urls.py than your have room in memory, you have other problems."""

    def __init__(self, contents, filename):
        self._contents = contents
        self._filename = filename

    def _replace(self, section, start_lineno, end_lineno, value):
        if isinstance(value, str) or not isinstance(value, Iterable):
            value = [
                value,
            ]
        return section[0:start_lineno] + value + section[end_lineno:]

    def append(self, selector, content):
        node = self.get_node(selector)
        if not node:
            raise SystemExit(f"Cannot insert {content}, could not find node for {selector}.")
        lines = self._contents.split("\n")
        if node.lineno == node.end_lineno:
            content = " " * (node.parent.col_offset - 1) + content
        else:
            content = " " * (node.end_col_offset - 1) + content
        lines.insert(node.end_lineno, content)
        self._contents = "\n".join(lines)

    def prepend(self, selector, content):
        node = self.get_node(selector)
        if not node:
            raise SystemExit(f"Cannot insert {content}, could not find node for {selector}.")
        lines = self._contents.split("\n")
        if node.lineno == node.end_lineno:
            pass
        else:
            content = " " * (node.end_col_offset - 1) + content
        lines.insert(node.lineno - 1, content)
        self._contents = "\n".join(lines)

    def insert(self, pos, selector, content):
        node = self.get_node(selector)
        if not node:
            raise SystemExit(f"Cannot insert {content}, could not find node for {selector}.")
        lines = self._contents.split("\n")
        if node.lineno == node.end_lineno:
            raise SystemExit("ERROR")

        content = " " * (node.end_col_offset + 3) + content + ","
        if pos < 0:
            pos = node.end_lineno + pos
        else:
            pos = node.lineno + pos
        lines.insert(pos, content)
        self._contents = "\n".join(lines)

    def get_node(self, selector):
        tree = ast.parse(self._contents, filename=self._filename)

        nv = Visitor(selector)
        nv.visit(tree)

        return nv.found

    def add_import(self, name, module=""):
        contents_lines = self._contents.split("\n")
        a = ast.parse(self._contents, filename=self._filename)
        if module:
            imports = [obj for obj in a.body if isinstance(obj, ast.ImportFrom) and obj.module == module]
        else:
            imports = [obj for obj in a.body if isinstance(obj, ast.Import)]

        for imp in imports:
            names = [alias.name for alias in imp.names]
            if name in names:
                return
            if module:
                names.append(name)
                val = f"from {module} import {', '.join(sorted(names))}"
                contents_lines = self._replace(
                    contents_lines,
                    imp.lineno,
                    imp.end_lineno,
                    [
                        val,
                    ],
                )
                self._contents = "\n".join(contents_lines)
                return
        if imports:
            start = imports[-1].lineno
            end = imports[-1].end_lineno
        else:
            # no other imports found, toss it at the top. Technically we could check for a docstring,
            # but that would probably be fragile without diving WAY farther into the python abstract grammar
            # than I really want to.
            start = 0
            end = 0
        if module:
            contents_lines = self._replace(contents_lines, start, end, f"from {module} import {name}")
        else:
            contents_lines = self._replace(contents_lines, start, end, f"import {name}")
        self._contents = "\n".join(contents_lines)

    def get_contents(self):
        return self._contents


def fresh_install(args):
    args.project_slug_name = re.sub(r"[^a-z]+", r"", args.name.lower())
    LOG.info("Creating new Django project %s...", args.project_slug_name)

    # call_command does a bunch of incidental django setup that will pollute the namespace.
    # since we later need to actually setup and use the namespace later after we've generated the
    # settings file, we fork to run startproject in it's own environment, which keeps the pollution
    # away from the rest of this execution.
    pid = os.fork()
    if pid == 0:
        # child fork
        call_command("startproject", args.project_slug_name)
        raise SystemExit(0)
    # parent
    os.waitpid(pid, 0)
    args.managepy = os.path.abspath(os.path.join(args.project_slug_name, "manage.py"))


def prep_existing_install(args):
    args.project_slug_name = os.path.basename(os.path.dirname(args.managepy))
    if os.path.isdir(args.managepy):
        # assuming they just gave us the project dir, try t find manage.py
        args.managepy = os.path.join(args.managepy, "manage.py")
    args.managepy = os.path.abspath(args.managepy)
    if not os.path.isfile(args.managepy):
        LOG.error("Could not find manage.py at %s.", args.managepy)
        raise SystemExit(-1)


def setup(args):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", f"{args.project_slug_name}.settings")
    os.chdir(os.path.dirname(args.managepy))
    sys.path.append(os.getcwd())

    if args.url:
        args.url = os.path.normpath(args.url)
    args.base_dir = os.path.dirname(args.managepy)


def plugins(args):
    if not os.path.exists("plugins"):
        LOG.info("Creating plugins folder...")
        os.mkdir("plugins")
        with open(os.path.join("plugins", "__init__.py"), "a", encoding="utf-8"):
            pass

    if os.path.exists(args.plugins):
        LOG.info("Installing plugins...")
        shutil.copytree(args.plugins, "plugins", dirs_exist_ok=True)


def urls(args):
    urlspy_path = os.path.join(args.base_dir, args.project_slug_name, "urls.py")
    LOG.info("Adding forum URLs to %s...", urlspy_path)
    with mod_file(urlspy_path) as f:
        f.add_import("include", module="django.urls")
        f.insert(-1, "urlpatterns", f'path("{args.url}", include("weaverforum.urls"))')


def mod_settings(args):
    settingspy_path = os.path.join(args.base_dir, args.project_slug_name, "settings.py")
    LOG.info("Adding forum settings to %s...", settingspy_path)
    with mod_file(settingspy_path) as f:
        f.insert(-1, "INSTALLED_APPS", '"weaverforum"')
        f.insert(-1, "MIDDLEWARE", '"weaverforum.middleware.CurrentlyOnlineMiddleware"')
        f.insert(-1, "MIDDLEWARE", '"weaverforum.middleware.CSPMiddleware"')

        f.insert(
            -1,
            "TEMPLATES[0]['OPTIONS']['context_processors']",
            '"weaverforum.forums.context_processors.SettingsProcessor"',
        )
        f.prepend(
            "TEMPLATES[0]['OPTIONS']['context_processors']",
            '"builtins": ["weaverforum.core.templatetags.weaver_display"],',
        )

        if not f.get_node("LOGIN_REDIRECT_URL"):
            f.append("STATIC_URL", 'LOGIN_REDIRECT_URL = "weaver_index"')
        if not f.get_node("MEDIA_URL"):
            f.append("STATIC_URL", 'MEDIA_URL = "/media/"')
        if not f.get_node("MEDIA_ROOT"):
            f.append("STATIC_URL", 'MEDIA_ROOT = "media/"')


def django_models(args):
    # External Dependencies
    import django  # pylint: disable=import-outside-toplevel

    django.setup()

    LOG.info("Migrating database changes...")
    call_command("migrate")

    if args.command == "fresh-install":
        LOG.info("Creating primary admin user...")
        call_command("createsuperuser")

    # Weaver Libraries
    from weaverforum.models import ForumConfiguration, Theme  # pylint: disable=import-outside-toplevel

    LOG.info("Setting Forum Configuration...")
    fc, _created = ForumConfiguration.objects.get_or_create(id=1)
    fc.forum_name = args.name
    fc.new_user_approval = not args.free_for_all
    fc.allow_guest_read = args.guest_read
    fc.save()

    LOG.info("Copying themes...")
    if args.themes:
        for css_file in os.listdir(args.themes):
            if not css_file.endswith(".css"):
                continue
            with open(css_file, "r", encoding="utf-8") as f:
                name = css_file.replace(".css", "")
                Theme.objects.create(name=name, css=f.read())


def do_install(args):
    LOG.info("Installing Weaver into %s...", args.project_slug_name)
    LOG.debug(args)
    setup(args)
    plugins(args)
    urls(args)
    mod_settings(args)
    django_models(args)
    LOG.info("\n - Complete --")


def print_base_help(parser):
    parser.print_usage()
    LOG.error(
        "\ncommands:\n"
        "    fresh-install       Create a brand new Weaver Forum from scratch.\n"
        "    existing-install    Add a Weaver Forum to an existing Django project.\n\n"
        "'weaverforum <command> --help' for full list of available flags."
    )
    raise SystemExit(-1)


def main():
    """Entry Point of the CLI interface."""
    parser = argparse.ArgumentParser(
        description="Utility command to manage and install WeaverForum.",
        usage="%(prog)s <command> [<args>...]",
    )
    parser.formatter_class = lambda prog: argparse.HelpFormatter(prog, max_help_position=30)
    parser.add_argument(
        "-n",
        "--name",
        help='Define the name of the forum. (Default: "Weaver Forum")',
        action="store",
        default="Weaver Forum",
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version=f"%(prog)s {VERSION}",
        help="Show program's version number and exit.",
    )
    parser.add_argument("-v", "--verbose", help="Show more log information.", action="count")
    parser.add_argument(
        "-g",
        "--guest-read",
        action="store_true",
        default=False,
        help="Guests are allowed to read the forum without registering. (Default: False)",
    )
    parser.add_argument(
        "-a",
        "--free-for-all",
        action="store_true",
        default=False,
        help="New users will not require approval. (Default: False)",
    )
    parser.add_argument(
        "-p",
        "--plugins",
        help="Path to directory of plugins to include.",
        action="store",
        metavar="PATH",
        default="",
    )
    parser.add_argument(
        "-u",
        "--url",
        action="store",
        metavar="PATH",
        default="",
        help='Install Weaver URLs into a sub-URL, e.g. "forums/" (Default: "")',
    )
    parser.add_argument(
        "-t",
        "--themes",
        default="",
        help="Path to directory of themes to be pre-installed.",
        action="store",
        metavar="PATH",
    )
    parser.add_argument("--debug", action="store_true", help=argparse.SUPPRESS)

    if len(sys.argv) < 2:
        print_base_help(parser)
    command = sys.argv[1]

    if command == "existing-install":
        parser.add_argument(
            "managepy",
            help="Path to existing project's manage.py.",
            action="store",
            metavar="PATH",
        )
        parser.usage = "%(prog)s existing-install PATH [<args>...]"
    elif command == "fresh-install":
        parser.usage = "%(prog)s fresh-install [<args>...]"
    else:
        print_base_help(parser)

    args = parser.parse_args(sys.argv[2:])
    args.command = command

    if args.verbose:
        LOG.setLevel(logging.INFO)
    if args.debug:
        LOG.setLevel(logging.DEBUG)
        LOG.debug("Debug mode enabled.")

    if not args.command in ["fresh-install", "existing-install"]:
        parser.print_help()

    if args.command == "fresh-install":
        fresh_install(args)
    else:
        prep_existing_install(args)

    do_install(args)


if __name__ == "__main__":
    main()

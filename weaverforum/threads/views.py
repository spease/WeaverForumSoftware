# Weaver Libraries
from weaverforum.core.decorators import guest_or_login_required, pprint_view
from weaverforum.forums.models import Forum, PostReport
from weaverforum.plugins.models import Plugin
from weaverforum.threads.forms import EditPostForm, PostReportForm, QuickPostForm, ThreadForm
from weaverforum.threads.models import Post, Thread

# External Dependencies
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse


@pprint_view(lambda x: f"Reading {get_object_or_404(Thread, pk=x.get('thread_id')).title}", url=True)
@guest_or_login_required
def view_thread(request, thread_id):
    thread = get_object_or_404(Thread, pk=thread_id)
    if (
        thread.state == 1 and not thread.forum.can_do(request.user, "can_view_deleted")
    ) or not thread.forum.can_do(
        request.user, "can_view"
    ):  # Deleted
        raise Http404
    form = Plugin.get_form(QuickPostForm, data=request.POST or None)
    if request.method == "POST":
        if not request.user.is_authenticated:
            return HttpResponseRedirect(f"{settings.LOGIN_URL}?next={request.path}")
        if not thread.forum.can_do(request.user, "can_post", thread):
            return Http404
        if form.is_valid():
            new_post = Post(
                created_by=request.user,
                text=form.cleaned_data["text"].strip(),
                thread=thread,
                state=0,
            )
            new_post.save()
            jump = f"?page={new_post.get_page()}#{new_post.id}"
            return HttpResponseRedirect(reverse("view_thread", args=[thread_id]) + jump)
    post_list = (
        Post.objects.filter(thread=thread)
        .order_by("created_on")
        .select_related("created_by", "created_by__profile", "created_by__profile__rank")
        .prefetch_related("created_by__posts")
    )
    if not (
        thread.forum.can_do(request.user, "can_view_deleted") and request.session.get("view_deleted", False)
    ):
        post_list = post_list.exclude(state=1)
    paginator = Paginator(list(post_list), 25)

    page = request.GET.get("page", None)
    post = request.GET.get("post", None)
    if not page and not post:
        page = 1
    if post:
        post = get_object_or_404(Post, pk=post)
        page = post.get_page()
    if page == "last":
        page = paginator.num_pages
    page_obj = paginator.get_page(page)
    metapost = page_obj[0]
    meta = f"{metapost.created_by.username}: {metapost.text}"
    meta = "\n".join(meta.split("\n")[:3])[:220]
    context = {
        "page_obj": page_obj,
        "thread": thread,
        "form": form,
        "metadescription": meta,
    }
    return Plugin.render_view("view_thread", request, "threads/view_thread.html", context)


@pprint_view(lambda x: f"Posting in {get_object_or_404(Thread, pk=x.get('thread_id')).title}", url=True)
@login_required
def reply_post(request, thread_id, post_id=None):
    thread = get_object_or_404(Thread, pk=thread_id)
    # If ?preview is appended, preview = ''. So we just invert the logic to get what we want
    preview = not request.GET.get("preview", True)
    try:
        qpost = Post.objects.get(pk=post_id)
        qpost = {"text": f"[quote={qpost.created_by.username}]\n{qpost.text}\n[/quote]\n"}
    except Post.DoesNotExist:
        qpost = None
    form = Plugin.get_form(EditPostForm, data=request.POST or qpost)
    if thread.state == 1 or not thread.forum.can_do(request.user, "can_view"):  # Deleted
        raise Http404

    context = {"form": form, "thread": thread, "qpost": qpost, "preview": preview}
    return Plugin.render_view("reply_post", request, "threads/reply_post.html", context=context)


@pprint_view(lambda x: f"Posting in {get_object_or_404(Forum, pk=x.get('forum_id')).title}")
@login_required
def post_thread(request, forum_id):
    forum = get_object_or_404(Forum, pk=forum_id)
    # If ?preview is appended, preview = ''. So we just invert the logic to get what we want
    preview = not request.GET.get("preview", True)
    if not forum.can_do(request.user, "can_view") or not forum.can_do(request.user, "can_post"):
        raise Http404
    form = Plugin.get_form(ThreadForm, data=request.POST or None)
    if request.method == "POST" and not preview:
        if form.is_valid():
            new_thread = Thread.objects.create(
                forum=forum,
                title=form.cleaned_data["title"].strip(),
                created_by=request.user,
                state=0,
            )
            first_post = Post.objects.create(
                created_by=request.user,
                text=form.cleaned_data["text"].strip(),
                thread=new_thread,
                state=0,
            )
            new_thread.first_post = first_post
            new_thread.save()
            return HttpResponseRedirect(reverse("view_thread", args=[new_thread.pk]))
    context = {"form": form, "forum": forum, "preview": preview}
    return Plugin.render_view("post_thread", request, "threads/post_thread.html", context=context)


@pprint_view("Editing Post")
@login_required
def edit_post(request, post_id):
    post = get_object_or_404(
        (Post.objects.select_related("thread", "thread__forum").prefetch_related("thread__raw_posts")),
        pk=post_id,
    )
    # If ?preview is appended, preview = ''. So we just invert the logic to get what we want
    preview = not request.GET.get("preview", True)
    if post.state == 1 or not post.thread.forum.can_do(request.user, "can_edit", post):
        raise Http404
    if post.is_first_to:
        form = Plugin.get_form(
            ThreadForm,
            data=request.POST or {"text": post.text, "title": post.thread.title},
        )
    else:
        form = Plugin.get_form(EditPostForm, data=request.POST or {"text": post.text})
    if request.method == "POST" and not preview:
        if form.is_valid():
            post.text = form.cleaned_data["text"]
            post.edited_by = request.user
            if post.is_first_to:
                post.thread.title = form.cleaned_data["title"]
                post.thread.save()
            post.save()
            jump = f"?page={post.get_page()}#{post_id}"
            return HttpResponseRedirect(reverse("view_thread", args=[post.thread.pk]) + jump)
    context = {"form": form, "post": post, "preview": preview}
    return Plugin.render_view("edit_post", request, "threads/edit_post.html", context=context)


@pprint_view("Editing Post")
@login_required
def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    thread = post.thread

    if not thread.forum.can_do(request.user, "can_delete", post):
        raise Http404
    thread_id = post.thread_id
    page = post.get_page()
    do_delete = True
    if post.state == 1:  # We're undeleting
        do_delete = False

    # If this is the first post
    if do_delete:
        posts = list(thread.posts.all())
    else:
        posts = list(thread.raw_posts.order_by("-created_on").all())
    if posts[-1].pk == post_id:  # If we're the first post, delete the thread, not the post.
        thread.state = 1 if do_delete else 0
        thread.save()
    elif posts[0].pk == post_id:  # If we're un/deleting the last post, update the thread.
        post.state = 1 if do_delete else 0
        post.save()
        if do_delete:
            post = posts[1]
        thread.last_post_by = post.created_by
        thread.last_post_on = post.created_on
        thread.save()
    else:  # just delete the post.
        post.state = 1 if do_delete else 0
        post.save()
    jump = f"?page={page}#last"
    return HttpResponseRedirect(reverse("view_thread", args=[thread_id]) + jump)


@pprint_view("Editing Post")  # Probably shouldn't announce this one.
@login_required
def report_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if post.state == 1:
        raise Http404
    form = Plugin.get_form(PostReportForm, data=request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            new_report = PostReport(
                reported_by=request.user,
                report=form.cleaned_data["report"].strip(),
                post=post,
            )
            new_report.save()

            jump = f"?page={post.get_path()}#{post_id}"
            return HttpResponseRedirect(reverse("view_thread", args=[post.thread.pk]) + jump)
    context = {"form": form, "post": post}
    return Plugin.render_view("report_post", request, "threads/report_post.html", context=context)

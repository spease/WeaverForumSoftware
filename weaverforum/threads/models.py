# Weaver Libraries
from weaverforum.core.models import Substitution

# External Dependencies
from django.conf import settings
from django.db import models


class Post(models.Model):
    id = models.BigAutoField(primary_key=True)
    POST_STATE_CHOICES = (
        (0, "Active"),
        (1, "Deleted"),
        (2, "VoiceOfGod"),
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="posts",
        on_delete=models.PROTECT,
    )
    created_on = models.DateTimeField(auto_now_add=True)
    edited_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="post_edits",
        on_delete=models.PROTECT,
        blank=True,
    )
    edited_on = models.DateTimeField(auto_now=True)
    text = models.TextField()
    thread = models.ForeignKey("Thread", null=False, related_name="raw_posts", on_delete=models.PROTECT)
    state = models.SmallIntegerField(choices=POST_STATE_CHOICES, default=0, db_index=True)

    def __str__(self):
        return f"[{self.created_by.username}] {self.text[:64]}"

    def get_page(self):
        return int(
            Post.objects.filter(thread__id=self.thread_id, created_on__lt=self.created_on)
            .exclude(state=1)
            .order_by("-created_by")
            .count()
            / 25
            + 1
        )

    def save(self, *args, **kwargs):
        update_thread = False
        if not self.pk:
            update_thread = True
        try:
            subs = Substitution.load()
            self.text = subs["P"].sub(lambda match: subs["PD"][match.group(0).lower()], self.text)
        except KeyError:
            pass
        super().save(*args, **kwargs)
        self.created_by.profile.recalculate_rank()
        if update_thread:
            self.thread.last_post_by = self.created_by
            self.thread.last_post_on = self.created_on
            self.thread.save()


class Thread(models.Model):
    id = models.BigAutoField(primary_key=True)
    THREAD_STATE_CHOICES = (
        (0, "Active"),
        (1, "Deleted"),
        (2, "Locked"),
    )
    forum = models.ForeignKey(
        "weaverforum.Forum",
        null=False,
        related_name="raw_threads",
        on_delete=models.PROTECT,
    )
    title = models.CharField(max_length=64, null=False, blank=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="threads",
        on_delete=models.PROTECT,
    )
    created_on = models.DateTimeField(auto_now_add=True)
    edited_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="thread_edits",
        on_delete=models.PROTECT,
        blank=True,
    )
    edited_on = models.DateTimeField(auto_now=True)
    last_post_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.PROTECT, blank=True
    )
    last_post_on = models.DateTimeField(db_index=True, null=True, blank=True)
    state = models.SmallIntegerField(choices=THREAD_STATE_CHOICES, default=0, db_index=True)
    pinned = models.BooleanField(default=False)
    first_post = models.ForeignKey(
        Post,
        null=True,
        related_name="is_first_to",
        on_delete=models.PROTECT,
        blank=True,
    )

    def save(self, *args, **kwargs):
        try:
            subs = Substitution.load()
            self.title = subs["P"].sub(lambda match: subs["PD"][match.group(0).lower()], self.title)
        except KeyError:
            pass
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    @property
    def posts(self):
        return self.raw_posts.exclude(state=1).order_by("-created_on")

    @property
    def long_title(self):
        ret = ""
        if self.pinned:
            ret += "★"
        if self.state == 2:
            ret += "🔒︎"
        if self.state == 1:
            ret += "[Deleted]"
        ret += " "
        return ret + self.title

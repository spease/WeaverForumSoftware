#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import logging

# Weaver Libraries
from weaverforum.threads.tests.factories import PostFactory

# External Dependencies
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse


class ViewThreadDNETests(TestCase):
    user = None

    def setUp(self):
        logging.disable(logging.CRITICAL)

    def test_not_logged_in(self):
        """If we aren't logged in, make sure we get a 302"""
        response = self.client.get(
            reverse(
                "view_thread",
                args=[
                    9999,
                ],
            )
        )
        self.assertEqual(response.status_code, 302)

    def test_thread_dne(self):
        """If the thread doesn't exist, make sure we get a 404"""
        self.user = User.objects.create_user("weaver", "weaver@example.com", "password")
        self.client.login(username="weaver", password="password")  # nosec
        response = self.client.get(
            reverse(
                "view_thread",
                args=[
                    9999,
                ],
            )
        )
        self.assertEqual(response.status_code, 404)


class ViewThreadTests(TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        self.user = User.objects.create_user("weaver", "weaver@example.com", "password")
        self.client.login(username="weaver", password="password")  # nosec
        self.active_thread_post = PostFactory.create(state=0)
        self.poster = self.active_thread_post.created_by
        self.active_thread_post.thread.state = 0
        self.active_thread_post.thread.save()
        PostFactory.create_batch(50, state=0, thread=self.active_thread_post.thread, created_by=self.poster)
        PostFactory.create(state=1, thread=self.active_thread_post.thread, created_by=self.poster)
        self.deleted_thread_post = PostFactory.create(state=1, created_by=self.poster)
        self.deleted_thread_post.thread.state = 0
        self.deleted_thread_post.thread.save()
        self.locked_thread_post = PostFactory.create(state=2, created_by=self.poster)
        self.locked_thread_post.thread.state = 0
        self.locked_thread_post.thread.save()

    def test_view_thread(self):
        response = self.client.get(
            reverse(
                "view_thread",
                args=[
                    self.active_thread_post.id,
                ],
            )
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            reverse(
                "view_thread",
                args=[
                    self.deleted_thread_post.id,
                ],
            )
        )
        self.assertEqual(response.status_code, 404)

        response = self.client.get(
            reverse(
                "view_thread",
                args=[
                    self.active_thread_post.id,
                ],
            )
            + "?post="
            + str(self.active_thread_post.id)
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            reverse(
                "view_thread",
                args=[
                    self.active_thread_post.id,
                ],
            )
            + "?page=last"
        )
        self.assertEqual(response.status_code, 200)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.threads.models import Post, Thread

# External Dependencies
import factory
import factory.django


class ThreadFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = Thread

    forum = factory.SubFactory("weaverforum.forums.tests.factories.ForumFactory")
    title = factory.Faker("sentence")
    state = factory.Iterator([x[0] for x in Thread.THREAD_STATE_CHOICES])
    pinned = factory.Faker("boolean")


class PostFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = Post

    created_by = factory.SubFactory("weaverforum.profiles.tests.factories.UserFactory")
    created_on = factory.Faker("date")
    text = factory.Faker("text")
    thread = factory.SubFactory(ThreadFactory)
    state = factory.Iterator([x[0] for x in Post.POST_STATE_CHOICES])

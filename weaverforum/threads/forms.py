#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.core.forms import PostArea, WeaverForm

# External Dependencies
from django import forms


class QuickPostForm(WeaverForm):
    ptext = "Please keep our forum rules in mind when contributing to this discussion."
    text = forms.CharField(
        widget=forms.Textarea(attrs={"placeholder": ptext, "id": "js-quickpost"}),
        label="Post",
        required=True,
    )


class ThreadForm(WeaverForm):
    ptext = "Please keep our forum rules in mind when contributing to this discussion."
    title = forms.CharField(
        required=True,
        label="Thread Title",
        max_length=64,
        widget=forms.TextInput(attrs={"placeholder": "Thread Title", "id": "id_title"}),
    )
    text = forms.CharField(
        widget=PostArea(attrs={"placeholder": ptext, "id": "large-thread-box"}),
        required=True,
        label="Post",
    )


class EditPostForm(WeaverForm):
    ptext = "Please keep our forum rules in mind when contributing to this discussion."
    text = forms.CharField(
        widget=PostArea(attrs={"placeholder": ptext, "id": "large-thread-box"}),
        required=True,
        label="Post",
    )


class PostReportForm(WeaverForm):
    report = forms.CharField(
        required=True,
        label="Report Reason",
        max_length=64,
        widget=forms.TextInput(attrs={"placeholder": "I am reporting this because..."}),
    )

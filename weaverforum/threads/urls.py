#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.threads import views

# External Dependencies
from django.urls import path

urlpatterns = [
    path("<int:thread_id>", views.view_thread, name="view_thread"),
    path("post/<int:forum_id>", views.post_thread, name="post_thread"),
    path("edit/<int:post_id>", views.edit_post, name="edit_post"),
    path("reply/<int:thread_id>", views.reply_post, name="reply_post"),
    path("quote/<int:thread_id>/<int:post_id>", views.reply_post, name="quote_post"),
    path("report/<int:post_id>", views.report_post, name="report_post"),
    path("delete/<int:post_id>", views.delete_post, name="delete_post"),
]

"""Top-level module for Weaver. Sets version."""

# Weaver Libraries
from weaverforum.version import VERSION

__version__ = VERSION
__version_info__ = tuple(int(i) for i in __version__.split(".") if i.isdigit())

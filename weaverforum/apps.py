# External Dependencies
from django.apps import AppConfig
from django.conf import settings

settings.INSTALLED_APPS.append("weaverforum.load_plugins")


class WeaverforumConfig(AppConfig):
    name = "weaverforum"
    verbose_name = "WeaverForum"

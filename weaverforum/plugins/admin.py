# External Dependencies
from django.contrib import admin


class WeaverPluginFileAdmin(admin.ModelAdmin):
    readonly_fields = ("name",)
    fields = ("name", "status", "plugin_options")
    list_display = ("name", "status")

    def options_helptext(self, *args, **kwargs):
        return args[0].get_options_helptext(*args, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        if obj and obj.status != 0:
            form.base_fields["status"].choices = [(1, "Disabled"), (2, "Enabled")]
        if obj:
            form.base_fields["plugin_options"].help_text = self.options_helptext(obj, request, **kwargs)
        if request.POST:
            # We need to reset permissions, this forces them to be evaluated despite appearing to do nothing
            request.user  # pylint: disable=pointless-statement
        return form

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if obj and obj.status == 0:
            readonly_fields = readonly_fields + ("status",)
        return readonly_fields


class PluginModelAdmin(admin.ModelAdmin):
    def has_view_permission(self, request, obj=None):
        if self.model.get_plugin(include_inactive=True).status != 2:
            return False
        return super().has_view_permission(request, obj)

    def has_add_permission(self, request):
        if self.model.get_plugin(include_inactive=True).status != 2:
            return False
        return super().has_view_permission(request)

    def has_change_permission(self, request, obj=None):
        if self.model.get_plugin(include_inactive=True).status != 2:
            return False
        return super().has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        if self.model.get_plugin(include_inactive=True).status != 2:
            return False
        return super().has_delete_permission(request, obj)

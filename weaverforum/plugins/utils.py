#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
from functools import wraps

# Weaver Libraries
from weaverforum.plugins.models import Plugin

# External Dependencies
from django.template import engines


def register_view(view_name):
    def decorate(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        wrapper.plugin_registration = "views"
        wrapper.plugin_selector = view_name
        return wrapper

    return decorate


def register_form(form_name):
    def decorate(cls):
        class Wrapper(cls):  # pylint: disable=too-few-public-methods
            pass

        Wrapper.plugin_registration = "forms"
        Wrapper.plugin_selector = form_name
        return Wrapper

    return decorate


def register_template(template_name):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            new_template = engines["django"].from_string(func(*args, **kwargs))
            if template_name not in Plugin.cached_templates:
                Plugin.cached_templates[template_name] = []
            Plugin.cached_templates[template_name].append(new_template)

        wrapper.plugin_registration = "templates"
        wrapper.plugin_selector = template_name
        return wrapper

    return decorator

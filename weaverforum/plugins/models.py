# Weaver Libraries
from weaverforum.core.models import Theme
from weaverforum.plugins.admin import WeaverPluginFileAdmin

# External Dependencies
from django.conf import settings
from django.contrib import admin
from django.db import models
from django.db.utils import OperationalError
from django.shortcuts import render


class PluginException(Exception):
    pass


class PluginClassCreator(type):
    # pylint: disable=no-value-for-parameter

    def __init__(cls, name, bases, attrs):  # pylint: disable=unused-argument
        if not hasattr(cls, "plugins"):
            # This branch only executes when processing the mount point itself.

            # So, since this is a new plugin type, not an implementation, this
            # class shouldn't be registered as a plugin. Instead, it sets up a
            # list where plugins can be registered later.
            cls.init_plugin_cache()
            cls.plugins = {}
            cls.registration = {
                "views": {},
                "forms": {},
                "templates": {},
            }
            cls.models = []
            cls.models_ready = False
        else:
            # This must be a plugin implementation, which should be registered.
            # Simply appending it to the list is all that's needed to keep
            # track of it later.
            cls.plugins[name] = cls
            for _methodname, method in cls.__dict__.items():
                if hasattr(method, "plugin_registration"):
                    if name not in Plugin.registration[method.plugin_registration]:
                        Plugin.registration[method.plugin_registration][name] = []
                    Plugin.registration[method.plugin_registration][name].append(method)

    def is_model_migrated(cls, model):
        is_migrated = getattr(model, "is_migrated", None)
        if is_migrated is None:
            try:
                model.objects.get(id=0)
                is_migrated = True
            except OperationalError:  # missing a field, needs migrated.
                is_migrated = False
            except model.DoesNotExist:  # this is actually fine
                # we just don't have a record, but we were able to check the db, so it's migrated.
                # yes this is a hacky way of checking, but looking at the MigrationExecutor doesn't
                # catch if we have migrations needed that do not yet have a migration file-
                # e.g. we would never be able to run makemigrations in the first place.
                is_migrated = True
            setattr(model, "is_migrated", is_migrated)
        return is_migrated

    def load_models(cls):
        if cls.models_ready:
            return False

        if not cls.is_model_migrated(WeaverPluginFile):
            return False
        for _name, raw_plugin in cls.plugins.items():
            pluginfile, _created = WeaverPluginFile.objects.get_or_create(name=raw_plugin.__name__)
            raw_plugin.weaver_obj = pluginfile
            raw_plugin.options = pluginfile.plugin_options

            is_migrated = all(cls.is_model_migrated(model) for model in raw_plugin.models)
            if not is_migrated:
                new_status = 0  # unmigrated
            else:
                if pluginfile.status == 0:  # unmigrated
                    new_status = 1  # disabled
                else:
                    new_status = pluginfile.status

            changed = False
            if new_status != pluginfile.status:
                pluginfile.status = new_status
                changed = True
            if raw_plugin.sys_admin_locked and (not pluginfile.sys_admin_locked or pluginfile.status != 2):
                pluginfile.sys_admin_locked = raw_plugin.sys_admin_locked
                pluginfile.status = 2
                changed = True
            if changed:
                pluginfile.save()

            raw_plugin.status = pluginfile.status
        cls.models_ready = True
        cls.cache_active_plugins()
        return True

    def init_plugin_cache(cls):
        cls.cached_views = {}
        cls.cached_forms = {}
        cls.cached_templates = {}
        cls.cached_css = ""
        cls.cached_js = ""

    def cache_active_plugins(cls, clear_first=False):
        if not cls.models_ready:
            return
        if clear_first:
            cls.init_plugin_cache()
        temp_css = ""
        temp_js = []
        for name, plugin in cls.plugins.items():
            if plugin.status != 2:
                continue
            for reg_type in ["views", "forms"]:
                for func in cls.registration[reg_type].get(name, []):
                    selector = func.plugin_selector
                    cache = getattr(cls, f"cached_{reg_type}")
                    if selector not in cache:
                        cache[selector] = []
                    cache[selector].append((func, plugin))

            # templates can be fully cached, right now
            for func in cls.registration["templates"].get(name, []):
                func(plugin)
            temp_css += plugin.plugin_css(plugin)
            temp_js.append(plugin.plugin_js(plugin))
        cls.cached_css = Theme.minify(temp_css)
        cls.cached_js = Theme.minify(";".join(temp_js))

    def get_plugins(cls, include_inactive=False):
        if not cls.models_ready:
            yield from ()
        for name, plugin in cls.plugins.items():
            if plugin.status != 2 and not include_inactive:
                continue
            yield name, plugin

    def get_models(cls):
        return cls.models

    @staticmethod
    def get_cache_by_name(cache, name, catchall_name):
        return cache.get(name, []) + cache.get(catchall_name, [])

    def render_view(cls, view_name, request, template, context, **kwargs):
        for func, plugin in cls.get_cache_by_name(cls.cached_views, view_name, "base"):
            request, template, temp_context = func(plugin, request, template, context)
            context.update(temp_context)
        return render(request, template, context, **kwargs)

    def get_form(cls, form_class, *args, **kwargs):
        classes = [form_class]
        attrs = dict(form_class.__dict__).copy()
        for form, _plugin in cls.get_cache_by_name(cls.cached_forms, form_class.__name__, "base"):
            classes.append(form)
        classes = tuple(classes[::-1])
        form_class = type(form_class.__name__ + "_WithPluginsAttached", classes, attrs)
        return form_class(*args, **kwargs)

    def get_css(cls, *args, **kwargs):
        return cls.cached_css

    def get_js(cls, *args, **kwargs):
        return cls.cached_js


class Plugin(metaclass=PluginClassCreator):
    """This is the class that actually holds individual loaded plugins. Any plugin created should
    inherit from this."""

    sys_admin_locked = False
    name = __name__

    def get_default_options(self, *args, **kwargs):
        """The default plugin options. This will be overridden by the plugin."""
        return {"options": None}

    def get_options_helptext(self, *args, **kwargs):
        """The default helptext for the options. This will be overridden by the plugin."""
        return "This plugin has no options."

    def plugin_css(self):
        """The default css for the plugin. Overwritten by the plugin."""
        return ""

    def plugin_js(self):
        """The default css for the plugin. Overwritten by the plugin."""
        return ""


class WeaverPluginFile(models.Model):
    """
    This is the representation of the plugin file. The only thing that creates WeaverPlugin objects is
    plugins.models.PluginClassCreator's load_models(), executed in weaverforum.apps's load_plugins(). The name
    field is set there, and status, if it needs auto-set, is also declared. Most of the time you do not use
    WeaverPlugin- this is the FILE, not the plugin, which is an instance of plugins.models.Plugin.
    """

    def get_default_options(self, *args, **kwargs):
        own_plugin = Plugin.plugins[self.name]  # pylint: disable=protected-access
        return own_plugin.get_default_options(self, *args, **kwargs)

    def get_options_helptext(self, *args, **kwargs):
        own_plugin = Plugin.plugins[self.name]  # pylint: disable=protected-access
        return own_plugin.get_options_helptext(self, *args, **kwargs)

    class Meta:  # pylint: disable=too-few-public-methods
        app_label = "load_plugins"
        verbose_name = "Weaver Plugin"
        verbose_name_plural = "- Weaver Plugins -"

    WEAVERPLUGIN_STATE_CHOICES = (
        (0, "Unmigrated"),
        (1, "Disabled"),
        (2, "Enabled"),
    )
    name = models.CharField(max_length=64, blank=False, null=False, editable=False)
    status = models.SmallIntegerField(choices=WEAVERPLUGIN_STATE_CHOICES, default=0)
    plugin_options = models.JSONField(blank=True, null=False)
    sys_admin_locked = models.BooleanField(default=False)
    code = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.status == 2:
            self.code = Plugin.plugins.get(self.name, None)
        if not getattr(settings, "register_plugin_admin", False):
            admin.site.register(self.__class__, WeaverPluginFileAdmin)
            settings.register_plugin_admin = True

    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        if not self.plugin_options:
            self.plugin_options = self.get_default_options()
        super().save(*args, **kwargs)
        if plugin := Plugin.plugins.get(self.name):
            plugin.status = self.status
            plugin.options = self.plugin_options
        Plugin.cache_active_plugins(clear_first=True)


class PluginModel(models.Model):
    """
    All plugins with database-backed information should inherit from this class. It acts similarly to
    models.Model.
    """

    class Meta:  # pylint: disable=too-few-public-methods
        app_label = "load_plugins"
        abstract = True

    urls = None
    model_admin = None
    models = []

    @classmethod
    def get_plugin(cls, include_inactive=False):
        names = [
            plugin_name
            for plugin_name, plugin in Plugin.get_plugins(include_inactive=include_inactive)
            if plugin.__module__ == cls.__module__
        ]
        return WeaverPluginFile.objects.get(name__in=names)

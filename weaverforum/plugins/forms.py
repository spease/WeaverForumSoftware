#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# External Dependencies
from django import forms


class PluginForm(forms.Form):
    """
    This is a dummy form for plugin forms to inherit. It's purpose it to act as a signifier/reminder/nudge
    to the plugin developer that they are inheriting other forms and need to use things like super().
    """

# Weaver Libraries
from weaverforum.core.models import ForumConfiguration
from weaverforum.core.views import report_csp
from weaverforum.forums.views import action_modcp, view_index, view_modcp, view_report
from weaverforum.profiles.views import register

# External Dependencies
from django.contrib.auth.views import (
    LoginView,
    PasswordResetCompleteView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
    logout_then_login,
)
from django.urls import include, path, re_path


def get_site_name():
    fc = ForumConfiguration.load()
    return fc.forum_name


urlpatterns = [
    path("", view_index, name="weaver_index"),
    path("modcp/", view_modcp, name="view_modcp"),
    path("modcp/<slug:action>/<int:thread_id>", action_modcp, name="action_modcp"),
    path("modcp/<int:report_id>", view_report, name="view_report"),
    path("forum/", include("weaverforum.forums.urls")),
    path("profiles/", include("weaverforum.profiles.urls")),
    path("thread/", include("weaverforum.threads.urls")),
    path("accounts/logout/", logout_then_login, name="logout"),
    path("accounts/login/", LoginView.as_view(), name="login"),
    path("accounts/register/", register, name="register"),
    path("report-csp-violation/", report_csp, name="report_csp"),
    path(
        "accounts/password_reset/",
        PasswordResetView.as_view(extra_email_context={"site_name": get_site_name}),
        name="password_reset",
    ),
    path(
        "accounts/password_reset/done/",
        PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    re_path(
        r"^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,40})/$",
        PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "accounts/reset/complete/",
        PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
]

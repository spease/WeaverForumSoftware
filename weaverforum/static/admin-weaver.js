function toggleAllPerms(perms) {
    perms.forEach(function (perm) {
        use_default_id = perm.id.slice(0, perm.id.lastIndexOf('-')+1) + 'use_default';
        use_default = document.getElementById(use_default_id);

        if (use_default.checked == true) {
            perm.readOnly = true;
        }
    });
}

function togglePerm(use_default) {
    selector = "[id^='" + use_default.id.slice(0, use_default.id.lastIndexOf('-')+1)+"']";
    perms = document.querySelectorAll(selector);
    perms.forEach(function (perm) {
        if (use_default.checked == true) {
            perm.readOnly = true;
            perm.className += ' unknown';
        } else {
            perm.readOnly = false;
            perm.className = perm.className.replace(/ unknown/, '');
        }
    });
}

document.addEventListener("DOMContentLoaded", () => {
    perms = document.querySelectorAll('.perms-class');
    defaults = document.querySelectorAll('.use-default');
    toggleAllPerms(perms);
    defaults.forEach(function(x) {
        x.addEventListener('change', function(e) {
            togglePerm(x);
        });
    });
});

"use strict";
/**
 * Turns Quote links into quickpost quotes
 * @returns {none} Nothing
 */
function QuotePost() {
    const quickpost = document.getElementById("js-quickpost");
    const text = document.getElementById(`js-post-${this.getAttribute("postid")}`).textContent.trim();

    quickpost.value += `[quote=${this.getAttribute("user")}]\n${text}\n[/quote]\n\n`;
    quickpost.focus();
}

function NewQuotePost(e) {
    e.preventDefault();
    window.open(this.getAttribute("old_href"), '_blank').focus();
}

/**
 * Wrap the cursor or the selection of el with the strings.
 * @param {element} el The textarea we're modifying
 * @param {string} startText The prepend string.
 * @param {string} endText The append string.
 * @returns {none} Nothing.
 */
function typeInTextArea(el, startText, endText) {
  var start = el.selectionStart;
  var end = el.selectionEnd;
  var text = el.value;
  var before = text.substring(0, start);
  var during = text.substring(start, end);
  var after  = text.substring(end, text.length);
  var newText = startText + during + endText;
  el.value = (before + newText + after);
  if (start == end) {
     el.selectionStart = el.selectionEnd = start + startText.length;
  } else {
     el.selectionStart = el.selectionEnd = start + newText.length;
  }
  el.focus();
}

document.addEventListener('DOMContentLoaded', function () {
    var quote_links = document.getElementsByClassName("js-quote-post");
    for (var i = 0, len = quote_links.length; i < len; i++) {
        quote_links[i].onclick = QuotePost;
        quote_links[i].onauxclick = NewQuotePost;
        quote_links[i].setAttribute('old_href', quote_links[i].href);
        quote_links[i].removeAttribute('href');
    }

    var dict = {"bb-size": "font-size", "bb-color": "color", "tag-color": "background-color"};
    for (var key in dict) {
        var spans = document.getElementsByClassName(key);
        for (var i = 0, len = spans.length; i < len; i++) {
            spans[i].style.setProperty(dict[key], spans[i].getAttribute(key));
        }
    }

    var pb_spans = document.getElementsByClassName("postbutton");
    var textarea = document.getElementsByName("text")[0];
    if (textarea) {
        for (let i = 0, len = pb_spans.length; i < len; i += 1) {
            pb_spans[i].removeAttribute("hidden");
            pb_spans[i].onclick = function() { window.typeInTextArea(textarea, pb_spans[i].getAttribute("pbstart"), pb_spans[i].getAttribute("pbend")) };
        }
    }
});

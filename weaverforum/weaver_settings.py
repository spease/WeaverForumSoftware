#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Standard Library
import socket
from os.path import basename, join, normpath

# External Dependencies
from django.conf import settings

HOST = socket.gethostname()

MEDIA_ROOT = getattr(settings, "WEAVER_THEME_ROOT", join(settings.MEDIA_ROOT, "weaver_themes/"))
MEDIA_URL = getattr(settings, "WEAVER_THEME_URL", join(settings.MEDIA_URL, "weaver_themes/"))
FROM_EMAIL = getattr(settings, "WEAVER_FROM_EMAIL", f"Weaver Notifications <notify@{HOST}>")
PLUGIN_DIR = getattr(settings, "WEAVER_PLUGIN_DIR", normpath(join(settings.BASE_DIR, "plugins/")))
PLUGIN_MODULE = getattr(settings, "WEAVER_PLUGIN_MODULE", basename(PLUGIN_DIR))

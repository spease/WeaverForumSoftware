# Standard Library
import os
import uuid

# External Dependencies
from django.conf import settings
from django.contrib.auth.models import User, UserManager
from django.contrib.sessions.models import Session
from django.core.cache import cache
from django.db import models
from django.utils.deconstruct import deconstructible


@deconstructible
class AvatarUpload:  # pylint: disable=too-few-public-methods
    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]
        unique = uuid.uuid4().hex
        filename = f"{unique}.{ext}"
        return os.path.join(self.sub_path, filename)


class UserProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="profile", primary_key=True
    )
    avatar = models.FileField(upload_to=AvatarUpload("weaver_avatars/"), blank=True)
    timezone = models.CharField(max_length=32, default="US/Eastern")
    rank = models.ForeignKey(
        "weaverforum.Rank",
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name="profiles",
    )
    theme = models.ForeignKey(
        "weaverforum.Theme",
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        limit_choices_to={"active": True},
    )
    ban_reason = models.CharField(max_length=128, null=False, blank=True, default="")

    def old_value(self, field, forcing_insert):
        if not self.pk or forcing_insert:
            return None
        own_manager = self.__class__._default_manager  # pylint: disable=protected-access
        return getattr(own_manager.get(pk=self.pk), field)

    def clear_sessions(self):
        for s in Session.objects.all():
            if s.get_decoded().get("_auth_user_id") == self.id:
                s.delete()

    def get_theme_id(self):
        if self.theme:
            return self.theme_id
        return 1

    def save(self, *args, **kwargs):
        old_value = self.old_value("avatar", kwargs.get("force_insert", False))
        changed = not getattr(self, "avatar") == old_value
        if self.avatar and changed:
            try:
                old_path = old_value.path
            except ValueError:
                old_path = False
            super().save(*args, **kwargs)
            if old_path:
                try:
                    os.remove(old_path)
                except OSError:
                    pass
        else:
            super().save(*args, **kwargs)

    @property
    def avatar_url(self):
        if self.avatar:
            return os.path.join(settings.MEDIA_URL, self.avatar.name)
        return ""

    def delete(self, *args, **kwargs):
        try:
            os.remove(self.avatar.path)
        except OSError:
            pass
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.user.username

    @property
    def is_mod(self):
        if self.user.is_staff:
            return True
        return self.rank.permissions.filter(can_mod=True).exists()

    @property
    def is_online(self):
        return self.user_id in cache.get("currently-online", [])

    def recalculate_rank(self):
        self.rank.recalculate_rank(self)


class AwaitingApprovalManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=False).filter(last_login=None)


class AwaitingApproval(User):
    objects = AwaitingApprovalManager()

    class Meta:  # pylint: disable=too-few-public-methods
        proxy = True


class PrivateMessage(models.Model):
    id = models.BigAutoField(primary_key=True)
    PM_STATE_CHOICES = (
        (0, "Unread"),
        (1, "Active"),
        (2, "Deleted"),
    )

    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="outbox",
        on_delete=models.PROTECT,
    )
    created_on = models.DateTimeField(auto_now_add=True)
    sent_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name="inbox",
        on_delete=models.PROTECT,
    )
    system_message = models.BooleanField(default=False)
    text = models.TextField()
    title = models.CharField(max_length=64, null=False, blank=False)
    sender_state = models.SmallIntegerField(choices=PM_STATE_CHOICES, default=1, db_index=True)
    receiver_state = models.SmallIntegerField(choices=PM_STATE_CHOICES, default=0, db_index=True)

    def __str__(self):
        return f"[{str(self.created_by)}] {str(self.text)[:64]}"

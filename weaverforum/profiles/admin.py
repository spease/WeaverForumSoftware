# Weaver Libraries
from weaverforum.profiles.models import AwaitingApproval, UserProfile

# External Dependencies
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe


class AwaitingApprovalAdmin(admin.ModelAdmin):
    fields = [
        "is_active",
    ]
    readonly_fields = ["email"]


class UserProfileInlineAdmin(admin.StackedInline):
    model = UserProfile

    fields = [
        "avatar",
        "avatar_preview",
        "timezone",
        "rank",
        "theme",
        "ban_reason",
    ]
    readonly_fields = [
        "avatar_preview",
    ]

    def avatar_preview(self, obj):
        return mark_safe(f'<img src="{obj.avatar.url}" width="64" height="64" />')  # nosec


class WeaverUserAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password", "is_active")}),
        ("Personal info", {"fields": ("first_name", "last_name", "email")}),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )
    inlines = (UserProfileInlineAdmin,)
    filter_horizontal = ()


admin.site.unregister(User)
admin.site.register(User, WeaverUserAdmin)
admin.site.register(AwaitingApproval, AwaitingApprovalAdmin)

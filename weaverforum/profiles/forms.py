#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.core.forms import AvatarField, PostArea, WeaverForm
from weaverforum.core.models import TIMEZONES, Theme

# External Dependencies
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class RegistrationForm(WeaverForm, UserCreationForm):  # pylint: disable=too-many-ancestors
    username = forms.CharField(
        max_length=150,
        required=True,
        label="Username",
        widget=forms.TextInput(attrs={"placeholder": "Username"}),
    )
    email = forms.EmailField(
        max_length=254,
        required=True,
        label="Email",
        widget=forms.TextInput(attrs={"placeholder": "Email"}),
    )
    confirm_email = forms.EmailField(
        max_length=254,
        required=False,
        label="Confirm Email",
        widget=forms.TextInput(
            attrs={
                "placeholder": "Email",
                "tabindex": "-1",
                "autocomplete": "new-password",
                "class": "hidden-post",
            }
        ),
    )
    password1 = forms.CharField(
        label="Password", widget=forms.PasswordInput(attrs={"placeholder": "Password"})
    )
    password2 = forms.CharField(
        label="Confirm Password",
        widget=forms.PasswordInput(attrs={"placeholder": "Password Confirmation"}),
    )

    class Meta:  # pylint: disable=too-few-public-methods
        model = User
        fields = (
            "username",
            "email",
            "password1",
            "password2",
        )


class EditSettingsForm(WeaverForm):
    def THEMES():  # pylint: disable=invalid-name,no-method-argument
        return [
            (None, "Default"),
        ] + [(x.pk, x.name) for x in Theme.objects.filter(active=True).order_by("id")]

    avatar = AvatarField(required=False)
    password1 = forms.CharField(
        label="Change Password",
        required=False,
        widget=forms.PasswordInput(attrs={"placeholder": "Change Password"}),
    )
    password2 = forms.CharField(
        label="Confirm Password",
        required=False,
        widget=forms.PasswordInput(attrs={"placeholder": "Confirm Password"}),
    )
    email = forms.EmailField(
        max_length=254,
        required=True,
        label="Email",
        widget=forms.TextInput(attrs={"placeholder": "Email"}),
    )
    timezone = forms.ChoiceField(label="Timezone", choices=TIMEZONES, required=True)
    theme = forms.ModelChoiceField(
        queryset=Theme.objects.filter(active=True),
        empty_label="Default",
        required=False,
    )

    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")

        if password1 or password2:
            if not (password1 and password2):
                self.add_error(
                    "password1",
                    forms.ValidationError("Both password fields are required."),
                )
            if not password1 == password2:
                self.add_error("password2", forms.ValidationError("Passwords must match."))
        return cleaned_data


class MessageForm(WeaverForm):
    ptext = "Please keep our forum rules in mind when messaging others."
    sent_to = forms.ModelChoiceField(
        required=True,
        label="Send To",
        disabled=True,
        queryset=User.objects.all(),
    )
    title = forms.CharField(
        required=True,
        label="Message Title",
        max_length=64,
        widget=forms.TextInput(attrs={"placeholder": "Message Title", "id": "id_title"}),
    )
    text = forms.CharField(
        widget=PostArea(attrs={"placeholder": ptext, "id": "large-thread-box"}),
        required=True,
        label="Message",
    )

# Weaver Libraries
from weaverforum.core.decorators import guest_or_login_required, pprint_view
from weaverforum.core.models import ForumConfiguration
from weaverforum.core.utils import send_mail
from weaverforum.plugins.models import Plugin
from weaverforum.profiles.forms import EditSettingsForm, MessageForm, RegistrationForm
from weaverforum.profiles.models import PrivateMessage, UserProfile

# External Dependencies
from django import forms
from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse


@pprint_view(lambda x: f"Viewing {get_object_or_404(User, pk=x.get('user_id')).username}'s profile", url=True)
@guest_or_login_required
def view_profile(request, user_id=None):
    if not user_id:
        user = get_object_or_404(User, pk=request.user_id)
    else:
        user = get_object_or_404(User, pk=user_id)
    context = {
        "profile": user.profile,
        "metadescription": "View {user.username}'s profile.",
    }
    return Plugin.render_view("view_profile", request, "profiles/view_profile.html", context)


@pprint_view("Viewing Member List", url=True)
@guest_or_login_required
def view_members(request):
    search = request.GET.get("msearch", None)
    page = request.GET.get("page", 1)
    if not search:
        users = User.objects.all()
    else:
        users = User.objects.filter(username__icontains=search)
    users = users.select_related("profile", "profile__rank").order_by("id")
    paginator = Paginator(users, 25)
    page_obj = paginator.get_page(page)
    context = {"page_obj": page_obj, "metadescription": "Member List"}
    return Plugin.render_view("view_members", request, "profiles/view_members.html", context)


@pprint_view("Viewing Currently Online", url=True)
@guest_or_login_required
def view_online(request):
    context = {"metadescription": "Currently Online"}
    return Plugin.render_view("view_online", request, "profiles/view_online.html", context)


@pprint_view("Viewing Inbox")
@login_required
def view_inbox(request):
    messages_list = (
        PrivateMessage.objects.filter(sent_to=request.user).exclude(receiver_state=2).order_by("-created_on")
    )
    paginator = Paginator(messages_list, 25)

    page = request.GET.get("page", 1)
    messages = paginator.get_page(page)
    context = {"messages": messages, "metadescription": "View Inbox", "type": "Inbox"}
    return Plugin.render_view("view_inbox", request, "profiles/view_messages.html", context)


@pprint_view("Viewing Outbox")
@login_required
def view_outbox(request):
    messages_list = (
        PrivateMessage.objects.filter(created_by=request.user)
        .exclude(receiver_state=2)
        .order_by("-created_on")
    )
    paginator = Paginator(messages_list, 25)

    page = request.GET.get("page", 1)
    messages = paginator.get_page(page)
    context = {"messages": messages, "metadescription": "View Outbox", "type": "Outbox"}
    return Plugin.render_view("view_outbox", request, "profiles/view_messages.html", context)


@pprint_view("Viewing Message")
@login_required
def view_message(request, message_id):
    message = get_object_or_404(PrivateMessage, pk=message_id)
    if message.sent_to == request.user:
        if message.receiver_state == 2:
            return Http404
        if message.receiver_state == 0:
            message.receiver_state = 1
            message.save()
    elif message.created_by == request.user:
        if message.sender_state == 2:
            return Http404
    else:
        return Http404

    context = {"message": message, "metadescription": "Viewing Private Message"}
    return Plugin.render_view("view_message", request, "profiles/view_message.html", context)


@pprint_view("Viewing Inbox")
@login_required
def delete_message(request, message_id):
    message = get_object_or_404(PrivateMessage, pk=message_id)
    if message.sent_to == request.user:
        message.receiver_state = 2
    if message.created_by == request.user:
        message.sender_state = 2
    message.save()
    return HttpResponseRedirect(reverse("view_inbox"))


@pprint_view("Viewing Inbox")
@login_required
def send_message(request, user_id, message_id=None):
    # If ?preview is appended, preview = ''. So we just invert the logic to get what we want
    preview = not request.GET.get("preview", True)
    user = get_object_or_404(User, pk=user_id)
    request.POST.sent_to = user.id
    form_initial = {"sent_to": user.id}

    if message_id:
        message = get_object_or_404(PrivateMessage, pk=message_id)
        form_initial.update(
            {
                "title": message.title,
                "text": f"[quote={message.created_by.username}]{message.text}[/quote]\n\n",
            }
        )

    form = Plugin.get_form(MessageForm, data=request.POST or None, initial=form_initial)

    if request.method == "POST" and not preview:
        if form.is_valid():
            new_message = PrivateMessage(
                sent_to=form.cleaned_data["sent_to"],
                title=form.cleaned_data["title"].strip(),
                created_by=request.user,
                text=form.cleaned_data["text"].strip(),
            )
            new_message.save()
            return HttpResponseRedirect(reverse("view_inbox"))
    context = {"form": form, "preview": preview, "sent_to": user}
    return Plugin.render_view("send_message", request, "profiles/send_message.html", context=context)


@pprint_view("Editing Profile")
@login_required
def edit_settings(request):
    theme = request.user.profile.theme
    fc = ForumConfiguration.load()
    if theme:
        theme = theme.pk
    defaults = {
        "email": request.user.email,
        "theme": theme,
        "timezone": request.user.profile.timezone,
        "request": request,
    }
    form = Plugin.get_form(EditSettingsForm, data=request.POST or defaults, files=request.FILES)

    if fc.avatar_max_size <= 0 or fc.avatar_max_dimensions <= 0:
        form.fields["avatar"].widget = forms.HiddenInput()
    msg = ""
    if request.method == "POST":
        if form.is_valid():
            profile = get_object_or_404(UserProfile, pk=request.user.profile.pk)
            profile.user.email = form.cleaned_data["email"]
            profile.timezone = form.cleaned_data["timezone"]
            profile.theme = form.cleaned_data["theme"]
            request.user.profile.theme = form.cleaned_data["theme"]
            if form.cleaned_data["password1"] and form.cleaned_data["password2"]:
                profile.user.set_password(form.cleaned_data["password1"])
            if fc.avatar_max_size > 0 and fc.avatar_max_dimensions > 0 and form.cleaned_data["avatar"]:
                profile.avatar = form.cleaned_data["avatar"]
            profile.user.save()
            profile.save()
            request.user.profile.avatar = profile.avatar
            msg = "Profile successfully saved."

    context = {"form": form, "msg": msg}
    return Plugin.render_view("edit_settings", request, "profiles/edit_settings.html", context)


def register(request):
    form = Plugin.get_form(RegistrationForm, data=request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            if form.cleaned_data["confirm_email"]:  # If a spambot tripped the honeypot
                return HttpResponseRedirect(reverse("registration_success"))
            fc = ForumConfiguration.load()
            is_active = not fc.new_user_approval
            user = get_user_model().objects.create_user(
                username=form.cleaned_data["username"],
                is_active=is_active,
                email=form.cleaned_data["email"],
                password=form.cleaned_data["password1"],
            )
            user.profile.timezone = fc.default_timezone
            user.profile.save()

            if is_active:
                username = form.cleaned_data.get("username")
                raw_password = form.cleaned_data.get("password1")
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return HttpResponseRedirect(reverse("view_forum_index"))
            send_mail(
                subject="New User Awaiting Approval",
                message=f"Hello! You have users awaiting approval at {request.get_host()}.",
                recipient_list=[u.email for u in get_user_model().objects.filter(is_staff=True)],
            )
            return HttpResponseRedirect(reverse("registration_success"))
    return Plugin.render_view("register", request, "profiles/register.html", {"form": form})


def registration_success(request):
    return Plugin.render_view("registration_success", request, "profiles/register_success.html", {})

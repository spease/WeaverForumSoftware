#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.profiles import views

# External Dependencies
from django.urls import path

urlpatterns = [
    path("", views.view_profile, name="view_profile"),
    path("<int:user_id>", views.view_profile, name="view_profile"),
    path("success", views.registration_success, name="registration_success"),
    path("edit", views.edit_settings, name="edit_settings"),
    path("list", views.view_members, name="view_members"),
    path("online", views.view_online, name="view_online"),
    path("inbox", views.view_inbox, name="view_inbox"),
    path("outbox", views.view_outbox, name="view_outbox"),
    path("messages/<int:message_id>", views.view_message, name="view_message"),
    path("messages/send/<int:user_id>", views.send_message, name="send_message"),
    path("messages/send/<int:user_id>/<int:message_id>", views.send_message, name="send_message"),
    path("messages/delete/<int:message_id>", views.delete_message, name="delete_message"),
]

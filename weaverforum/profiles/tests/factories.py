#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.profiles.models import UserProfile

# External Dependencies
import factory
import factory.django
from django.contrib.auth.models import Group, User
from django.db.models.signals import post_save


@factory.django.mute_signals(post_save)
class UserProfileFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = UserProfile

    user = factory.SubFactory("profiles.tests.factories.UserFactory", profile=None)
    rank = factory.SubFactory("weaverforum.core.tests.factories.RankFactory")
    timezone = factory.Faker("timezone")


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = Group

    name = factory.Sequence(lambda n: f"Group #{n}")


@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = User

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = factory.Faker("user_name")
    password = factory.Faker("password")
    email = factory.LazyAttribute(lambda a: f"{a.username.lower()}@example.com")
    profile = factory.RelatedFactory(UserProfileFactory, factory_related_name="user")
    is_active = True

    @factory.post_generation
    def groups(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for group in extracted:
                self.groups.add(group)

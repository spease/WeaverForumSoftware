#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.profiles.forms import EditSettingsForm

# External Dependencies
from django.test import TestCase


class ProfileFormsTests(TestCase):
    def test_forms(self):
        themes = [
            (None, "Default"),
            (1, "Weaver Light Theme"),
            (2, "Weaver Dark Theme"),
        ]
        self.assertTrue(EditSettingsForm.THEMES() == themes)
        data = {
            "email": "test@test.com",
            "timezone": "US/Eastern",
            "theme": None,
        }
        self.assertTrue(EditSettingsForm(data=data).is_valid())

        data = {
            "password1": "test",  # nosec
            "email": "test@test.com",
            "timezone": "US/Eastern",
            "theme": None,
        }
        self.assertFalse(EditSettingsForm(data=data).is_valid())

        data = {
            "password1": "test",  # nosec
            "password2": "nottest",  # nosec
            "email": "test@test.com",
            "timezone": "US/Eastern",
            "theme": None,
        }
        self.assertFalse(EditSettingsForm(data=data).is_valid())

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.core.forms import WeaverForm
from weaverforum.forums.models import PostReport

# External Dependencies
from django import forms


class JudgeReportForm(WeaverForm):
    def __init__(self, *args, **kwargs):
        perms = {
            "B": kwargs.pop("can_ban", False),
            "E": kwargs.pop("can_edit", False),
            "D": kwargs.pop("can_delete", False),
            "N": True,
        }
        super().__init__(*args, **kwargs)
        action_choices = []
        for flag, label in self.fields["action"].choices:
            if perms[flag]:
                action_choices.append((flag, label))
        self.fields["action"].choices = action_choices
        if len(action_choices) == 1:
            state_choices = [(flag, label) for flag, label in self.fields["state"].choices if flag != "A"]
            self.fields["state"].choices = state_choices

    state = forms.ChoiceField(label="State", choices=PostReport.REPORT_STATE_CHOICES, required=False)
    action = forms.ChoiceField(label="Action", choices=PostReport.REPORT_ACTION_CHOICES, required=False)
    action_reason = forms.CharField(
        label="Action Reason",
        max_length=128,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "I made this decision because..."}),
    )

    def clean(self):
        if self.cleaned_data["state"] != "O" and self.cleaned_data["action_reason"] == "":
            self.add_error(
                "action_reason",
                forms.ValidationError("You must give an action reason."),
            )
        if self.cleaned_data["state"] == "D" and self.cleaned_data["action"] != "N":
            self.add_error(
                "action",
                forms.ValidationError("You cannot take action when dismissing a report."),
            )
        if self.cleaned_data["state"] == "A" and self.cleaned_data["action"] == "N":
            self.add_error("action", forms.ValidationError("Please set your action."))
        return self.cleaned_data


class JudgeReportFormReadOnly(JudgeReportForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["state"].widget.attrs["disabled"] = True
        self.fields["action"].widget.attrs["disabled"] = True
        self.fields["action_reason"].widget.attrs["disabled"] = True

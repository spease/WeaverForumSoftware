# Weaver Libraries
from weaverforum.forums.models import Forum, ForumPermissionSet, PostReport

# External Dependencies
from django import forms
from django.contrib import admin


class ForumPermissionSetInlineForm(forms.ModelForm):
    class Meta:  # pylint: disable=too-few-public-methods
        model = ForumPermissionSet
        fields = (  # pylint: disable=duplicate-code
            "rank",
            "use_default",
            "can_view",
            "can_post",
            "can_edit_own",
            "can_edit_other",
            "can_delete_own",
            "can_delete_other",
            "can_view_deleted",
            "can_lock",
            "can_post_locked",
            "can_pin",
            "can_mod",
            "can_ban",
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            if "class" not in visible.field.widget.attrs:
                css = []
            else:
                css = visible.field.widget.attrs["class"].split(" ")
            if visible.name == "use_default":
                css.append("use-default")
            else:
                css.append("perms-class")
                visible.field.widget.attrs["original"] = "false"
            visible.field.widget.attrs["class"] = " ".join(css)

    def clean(self):
        # Horribly hacky way to keep roles from getting screwed.
        rank = self.cleaned_data["id"].rank
        only_perms = {
            x: self.cleaned_data[x]
            for x in self.cleaned_data.keys()
            if x not in ["forum", "id", "use_default"]
        }
        if rank.id == 4:  # Guest
            if any(only_perms[x] for x in only_perms.keys() if x not in ["can_view", "can_post"]):
                raise forms.ValidationError(
                    f"{rank.name} cannot be assigned permissions other than 'Can View' and 'Can Post.'"
                )
        elif rank.id == 1:  # Super Administrator
            if not all(only_perms[x] for x in only_perms.keys()):
                raise forms.ValidationError(f"{rank.name} must be assigned all permissions.")

        if not only_perms["can_view"] and any(
            only_perms[x] for x in only_perms.keys() if x not in ["can_view"]
        ):
            raise forms.ValidationError("'Can View' is required for other permissions to be assignable.")
        return self.cleaned_data


class ForumPermissionSetInline(admin.TabularInline):
    model = ForumPermissionSet
    form = ForumPermissionSetInlineForm
    extra = 0
    max_num = 0
    can_delete = False
    can_add = False
    ordering = ("rank__order_val",)
    fields = (  # pylint: disable=duplicate-code
        "rank",
        "use_default",
        "can_view",
        "can_post",
        "can_edit_own",
        "can_edit_other",
        "can_delete_own",
        "can_delete_other",
        "can_view_deleted",
        "can_lock",
        "can_post_locked",
        "can_pin",
        "can_mod",
        "can_ban",
    )
    readonly_fields = [
        "rank",
    ]

    class Media:  # pylint: disable=too-few-public-methods
        js = ("admin-weaver.js",)
        css = {"all": ("admin-weaver.css",)}


class ForumAdmin(admin.ModelAdmin):
    inlines = [
        ForumPermissionSetInline,
    ]


class PostReportAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    model = PostReport
    can_delete = False
    can_add = False
    fields = (
        "post",
        "report",
        "reported_by",
        "reported_on",
        "state",
        "action",
        "action_reason",
    )
    readonly_fields = ("post", "report", "reported_by", "reported_on")


admin.site.register(Forum, ForumAdmin)
admin.site.register(PostReport, PostReportAdmin)

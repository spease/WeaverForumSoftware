#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.core.models import BBcodeTag, ForumConfiguration, Substitution
from weaverforum.forums.models import Rank
from weaverforum.plugins.models import Plugin

# External Dependencies
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser


def SettingsProcessor(request):  # pylint: disable=invalid-name
    User = get_user_model()  # pylint: disable=invalid-name
    generated_css = Rank.get_css() + Plugin.get_css()
    generated_js = Plugin.get_js()
    try:
        user = User.objects.select_related("profile", "profile__rank").get(id=request.user.id)
        profile = user.profile
        rank = user.profile.rank
        unread = user.inbox.filter(receiver_state=0).count()
        if unread:
            unread = f" ({unread})"
        else:
            unread = ""
    except User.DoesNotExist:
        user = AnonymousUser
        profile = None
        rank = Rank.objects.get(type="G")
        unread = ""
    return {
        "settings": ForumConfiguration.load(),
        "generated_css": generated_css,
        "generated_js": generated_js,
        "subs": Substitution.load(),
        "tags": BBcodeTag.load(),
        "user": user,
        "profile": profile,
        "rank": rank,
        "unread": unread,
    }

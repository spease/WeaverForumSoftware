#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.forums.models import Forum

# External Dependencies
import factory
import factory.django


class ForumFactory(factory.django.DjangoModelFactory):
    class Meta:  # pylint: disable=too-few-public-methods
        model = Forum

    name = factory.Faker("sentence")
    description = factory.Faker("paragraph")
    state = 0
    order = factory.Faker("pyint")

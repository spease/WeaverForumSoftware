#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.forums.forms import JudgeReportForm, JudgeReportFormReadOnly

# External Dependencies
from django.test import TestCase


class ForumFormsTests(TestCase):
    def test_forms(self):
        can_do = {
            "can_ban": True,
            "can_edit": True,
            "can_delete": True,
        }
        good = JudgeReportForm(
            data={"state": "A", "action": "D", "action_reason": "Test"}, **can_do
        ).is_valid()
        self.assertTrue(good)
        reason = JudgeReportForm(data={"state": "A", "action": "D", "action_reason": ""}, **can_do).is_valid()
        self.assertFalse(reason)
        dismiss = JudgeReportForm(
            data={"state": "D", "action": "D", "action_reason": "Test"}, **can_do
        ).is_valid()
        self.assertFalse(dismiss)
        action = JudgeReportForm(
            data={"state": "A", "action": "N", "action_reason": "Test"}, **can_do
        ).is_valid()
        self.assertFalse(action)

        read = JudgeReportFormReadOnly(
            data={"state": "A", "action": "D", "action_reason": "Test"}, **can_do
        ).is_valid()
        self.assertTrue(read)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.forums.tests.factories import ForumFactory

# External Dependencies
from django.test import TestCase


class ForumTestCase(TestCase):
    def test_forum_ordering(self):
        """
        Test that forums are ordered correctly:
        first by order, then by name.
        """
        one = ForumFactory(order=100, name="a")
        two = ForumFactory(order=200)
        deleted = ForumFactory(order=100, name="b")

        self.assertTrue(one < two)
        self.assertTrue(one < deleted)
        self.assertTrue(deleted < two)

    def test_stringify(self):
        """
        Ensure that forum's str() is the name.
        """
        forum = ForumFactory()
        self.assertEqual(str(forum), forum.name)

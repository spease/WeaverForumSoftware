# Standard Library
import re
from datetime import datetime

# Weaver Libraries
from weaverforum.core.decorators import guest_or_login_required, pprint_view
from weaverforum.forums.forms import JudgeReportForm, JudgeReportFormReadOnly
from weaverforum.forums.models import Forum, PostReport
from weaverforum.plugins.models import Plugin
from weaverforum.threads.models import Post, Thread

# External Dependencies
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Count, Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse

SEARCH_RE = re.compile(r"((?:threads_by|post_by|in_thread|in_forum):[0-9]+) ?")


@pprint_view("Viewing Board Index", url=True)
@guest_or_login_required
def view_index(request):
    # 41 in 3.75
    forums = (
        Forum.objects.by_rank(request.user.profile.rank_id).filter(parent=None).prefetch_related("children")
    )
    meta = "Discuss and share!"
    context = {"forums": forums, "metadescription": meta}
    return Plugin.render_view("view_index", request, "forums/view_forum_index.html", context)


@pprint_view(lambda x: f"Browsing {get_object_or_404(Forum, pk=x.get('forum_id')).name}", url=True)
@guest_or_login_required
def view_forum(request, forum_id):
    forum = get_object_or_404(Forum, pk=forum_id)
    if not forum.can_do(request.user, "can_view"):
        raise Http404

    if not (forum.can_do(request.user, "can_view_deleted") and request.session.get("view_deleted", False)):
        thread_list = (
            forum.threads.select_related("created_by", "last_post_by")
            .exclude(state=1)
            .annotate(num_posts=Count("raw_posts", exclude=Q(state=1)))
        )
    else:
        thread_list = forum.threads.select_related("created_by", "last_post_by").annotate(
            num_posts=Count("raw_posts")
        )
    paginator = Paginator(thread_list, 25)

    page = request.GET.get("page", 1)
    threads = paginator.get_page(page)
    context = {"threads": threads, "forum": forum, "metadescription": forum.description}
    return Plugin.render_view("view_forum", request, "forums/view_forum.html", context)


def get_search_filters(request):
    search_request = request.GET.get("search", "")

    filtered_search_request = SEARCH_RE.sub("", search_request).strip()

    filter_map = {
        # None indicates we match off filter_id
        "post_by": {"created_by": None},
        "threads_by": {"thread__created_by": None, "is_first_to__isnull": False},
        "in_thread": {"thread": None},
        "in_forum": {"thread__forum": None},
    }
    filters = {}
    for match in SEARCH_RE.findall(search_request):
        filter_class, filter_id = match.split(":")
        subfilter = filter_map.get(filter_class, {})
        for key, item in subfilter.items():
            filters[key] = filter_id if item is None else item

    return search_request, filtered_search_request, filters


@pprint_view("Viewing Search Results")
@login_required
def view_search_results(request):
    query, filtered_query, filters = get_search_filters(request)
    forums_can_search = [f for f in Forum.objects.all() if f.can_do(request.user, "can_view")]

    results = Post.objects.none()
    page_obj = []
    too_short = False

    if query and len(query) > 4:
        # Filter out anything you don't have permission to read, was deleted, or was in a deleted thread.
        results = (
            Post.objects.select_related("thread", "created_by", "thread__forum")
            .filter(text__icontains=filtered_query)
            .filter(thread__forum__in=forums_can_search)
            .exclude(state=1)
            .exclude(thread__state=2)
            .filter(**filters)
            .order_by("-created_on")
        )
        paginator = Paginator(results, 25)

        page = request.GET.get("page", 1)
        page_obj = paginator.get_page(page)
    elif query and len(query) < 4:
        too_short = True

    context = {
        "page_obj": page_obj,
        "search_request": query,
        "filtered_search_request": filtered_query,
        "metadescription": "Search Results",
        "too_short": too_short,
    }
    return Plugin.render_view("view_search_results", request, "forums/view_search_results.html", context)


@pprint_view("Moderator Control Panel")
@login_required
def view_modcp(request, message=None, link=None):
    forums = Forum.objects.filter(
        id__in=request.user.profile.rank.permissions.filter(can_mod=True).values_list("forum", flat=True)
    )
    if not forums:
        raise Http404

    # Reports that are for posts in threads in forums current user can see.
    # reports select_related(created_by, post, post__thread post__created_by
    report_list = (
        PostReport.objects.filter(post__thread__forum__in=forums)
        .select_related("reported_by", "post", "post__created_by", "post__thread")
        .order_by("-state", "-reported_on")
    )
    show_all = request.GET.get("all", None) is not None
    if not show_all:
        report_list = report_list.filter(state="O")

    paginator = Paginator(report_list, 25)

    page = request.GET.get("page", 1)
    reports = paginator.get_page(page)
    context = {
        "reports": reports,
        "show_all": show_all,
        "message": message,
        "link": link,
    }
    return Plugin.render_view("view_modcp", request, "forums/view_modcp.html", context)


@pprint_view("Moderator Control Panel")
@login_required
def action_modcp(request, action=None, thread_id=None):
    if action == "modview":
        request.session["view_deleted"] = not request.session.get("view_deleted", False)
        msg = "Moderator mode {}abled. Deleted content is {} viewable."
        if request.session["view_deleted"]:
            msg = msg.format("en", "now")
        else:
            msg = msg.format("dis", "no longer")
        link = reverse("view_modcp")
        return view_modcp(request, msg, link)

    # If a forum and thread are needed.
    forums = Forum.objects.filter(
        id__in=request.user.profile.rank.permissions.filter(can_mod=True).values_list("forum", flat=True)
    )
    thread = get_object_or_404(Thread, pk=thread_id)
    link = reverse("view_thread", args=[thread.pk]) + "?page=last#last"
    if not forums:
        raise Http404
    msg = None
    if action == "bump" and thread.forum.can_do(request.user, "can_pin"):
        thread.last_post_on = datetime.now()
        thread.save()
        msg = "Thread '{}' has been bumped. Click here to be redirected."
        msg = msg.format(thread.title)
    elif action == "pin" and thread.forum.can_do(request.user, "can_pin"):
        msg = "Thread '{}' has been {}pinned. Click here to be redirected."
        msg = msg.format(thread.title, "un" if thread.pinned else "")
        thread.pinned = not thread.pinned
        thread.save()
    elif (
        action == "delete"
        and thread.forum.can_do(request.user, "can_delete_other")
        and thread.state in [0, 1]
    ):
        msg = "Thread '{}' has been {}deleted. Click here to be redirected."
        msg = msg.format(thread.title, "un" if thread.state == 1 else "")
        thread.state = 0 if thread.state == 1 else 1
        thread.save()
    elif action == "lock" and thread.forum.can_do(request.user, "can_lock") and thread.state in [0, 2]:
        msg = "Thread '{}' has been {}locked. Click here to be redirected."
        msg = msg.format(thread.title, "un" if thread.state == 2 else "")
        thread.state = 0 if thread.state == 2 else 2
        thread.save()
    else:
        raise Http404
    return view_modcp(request, msg, link)


def check_mod_perms(action, mod_perms):
    if action == "B" and not mod_perms["can_ban"]:
        raise Http404
    if action == "E" and not mod_perms["can_edit"]:
        raise Http404
    if action == "D" and not mod_perms["can_delete"]:
        raise Http404


@pprint_view("Moderator Control Panel")
@login_required
def view_report(request, report_id):

    report = get_object_or_404(
        PostReport.objects.select_related(
            "reported_by",
            "post",
            "post__created_by",
            "post__thread",
            "post__thread__forum",
        ),
        pk=report_id,
    )
    if not report.post.thread.forum.can_do(request.user, "can_mod"):
        raise Http404

    mod_perms = {
        "can_ban": report.post.thread.forum.can_do(request.user, "can_ban"),
        "can_delete": report.post.thread.forum.can_do(request.user, "can_delete"),
        "can_edit": report.post.thread.forum.can_do(request.user, "can_edit"),
    }

    if report.state == "O":
        form = Plugin.get_form(
            JudgeReportForm(
                request.POST
                or {
                    "state": report.state,
                    "action": report.action,
                    "action_reason": report.action_reason,
                }.update(mod_perms)
            )
        )
    else:
        form = Plugin.get_form(
            JudgeReportFormReadOnly,
            data={
                "state": report.state,
                "action": report.action,
                "action_reason": report.action_reason,
            },
        )
    if request.method == "POST" and report.state == "O":
        if form.is_valid():
            report.state = form.cleaned_data["state"]
            report.action = form.cleaned_data["action"]
            report.action_reason = form.cleaned_data["action_reason"]
            report.save()

            check_mod_perms(form.cleaned_data["action"], mod_perms)

            if form.cleaned_data["action"] == "B":
                user = report.post.created_by
                user.is_active = False
                user.profile.ban_reason = f"Report #{report.id} ({report.action_readon})."
                user.save()
                user.profile.clear_sessions()
                return HttpResponseRedirect(reverse("view_report", args=[report_id]))

            if form.cleaned_data["action"] == "D":
                return HttpResponseRedirect(reverse("delete_post", args=[report.post.id]))

            if form.cleaned_data["action"] == "E":
                return HttpResponseRedirect(reverse("edit_post", args=[report.post.id]))

            return HttpResponseRedirect(reverse("view_report", args=[report_id]))

    context = {"report": report, "form": form}
    return Plugin.render_view("view_report", request, "forums/view_report.html", context)

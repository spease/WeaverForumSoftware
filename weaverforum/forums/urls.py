#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Weaver Libraries
from weaverforum.forums import views

# External Dependencies
from django.urls import path

urlpatterns = [
    path("", views.view_index, name="view_forum_index"),
    path("<int:forum_id>", views.view_forum, name="view_forum"),
    path("search", views.view_search_results, name="view_search_results"),
]

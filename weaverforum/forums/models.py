# Weaver Libraries
from weaverforum.core.models import ForumConfiguration, Theme
from weaverforum.core.utils import send_mail
from weaverforum.profiles.models import UserProfile

# External Dependencies
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.cache import cache
from django.db import models
from django.db.models.query import QuerySet
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.forms import ValidationError
from django.utils.functional import cached_property


class ForumQuerySet(QuerySet):
    def by_rank(self, rank_id):
        return self.prefetch_related("permissions").filter(
            permissions__can_view=True, permissions__rank_id=rank_id
        )


class ForumManager(models.Manager):
    def get_queryset(self):
        return ForumQuerySet(self.model, using=self._db)

    def by_rank(self, rank_id):
        return self.get_queryset().by_rank(rank_id)


class Forum(models.Model):
    id = models.BigAutoField(primary_key=True)
    objects = ForumManager()
    FORUM_STATE_CHOICES = (
        (0, "Active"),
        (1, "Deleted"),
    )
    parent = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="children",
    )
    name = models.CharField(max_length=64, null=False, blank=False)
    description = models.CharField(max_length=256, null=False, blank=True)
    state = models.SmallIntegerField(choices=FORUM_STATE_CHOICES, default=0, db_index=True)
    order = models.IntegerField(null=False, blank=False)

    class Meta:  # pylint: disable=too-few-public-methods
        ordering = ["order", "name"]

    def __str__(self):
        return str(self.name)

    def __lt__(self, other):
        if self.order == other.order:
            return self.name < other.name
        return self.order < other.order

    def _clearcache(self, cache_name):
        try:
            delattr(self, cache_name)
        except AttributeError:
            pass

    def save(self, *args, **kwargs):
        self._clearcache("children_cache_shallow")
        self._clearcache("children_cache_depth")
        super().save(*args, **kwargs)
        for rank in Rank.objects.prefetch_related("permissions").all():
            try:
                rank.permissions.get(forum=self.id)
            except rank.permissions.model.DoesNotExist:
                ForumPermissionSet.objects.create(
                    forum_id=self.id,
                    rank_id=rank.id,
                    can_view=rank.can_view,
                    can_post=rank.can_post,
                    can_edit_own=rank.can_edit_own,
                    can_edit_other=rank.can_edit_other,
                    can_delete_own=rank.can_delete_own,
                    can_delete_other=rank.can_delete_other,
                    can_view_deleted=rank.can_view_deleted,
                    can_lock=rank.can_lock,
                    can_post_locked=rank.can_post_locked,
                    can_pin=rank.can_pin,
                    can_mod=rank.can_mod,
                    can_ban=rank.can_ban,
                )

    @property
    def threads(self):
        return self.raw_threads.order_by("-pinned", "-last_post_on")

    def get_rank(self, user):
        rank = getattr(user, "rank", None)
        if not rank:
            try:
                rank = user.profile.rank
            except AttributeError:
                rank = Rank.objects.get(type="G")
            setattr(user, "rank", rank)
        return rank

    def can_do(self, user, permission, obj=None):  # pylint: disable=too-many-return-statements
        rank = self.get_rank(user)
        fps = getattr(user, f"permissions_{self.id}", None)
        if not fps:
            fps = self.permissions.get(rank=rank)
            setattr(user, f"permissions_{self.id}", fps)
        if permission == "can_edit":
            # obj is post
            if obj and obj.state != 1:
                if obj.created_by_id == user.id:
                    return getattr(fps, "can_edit_own", False)
                return getattr(fps, "can_edit_other", False)
            return False
        if permission == "can_delete":
            # obj is post
            if obj:
                if obj.created_by_id == user.id:
                    return getattr(fps, "can_delete_own", False)
                return getattr(fps, "can_delete_other", False)
            return False
        if permission == "can_post":
            # obj is thread
            if obj and obj.state == 2:
                return getattr(fps, "can_post_locked", False)
            return getattr(fps, permission, False)
        return getattr(fps, permission, False)

    def get_depth_children(self, depth=False):
        cache_name = f'children_cache_{"depth" if depth else "shallow"}'
        children = getattr(self, cache_name, None)
        if not children:
            children = self.children.all()
            if depth:
                for child in children:
                    children.union(child.get_depth_children(depth=True))
            setattr(self, cache_name, children)
        return children

    def get_parents(self):
        parents = []
        parent = self.parent
        while parent:
            parents.append(parent)
            parent = parent.parent
        return parents


class Rank(models.Model):
    id = models.BigAutoField(primary_key=True)
    RANK_TYPE_CHOICES = (
        ("D", "Default"),
        ("P", "Assigned via Post Count"),
        ("M", "Manual Assignment Only (Will Not Be Overriden)"),
        ("G", "Guest"),
    )
    style_default = ".rankstyle-$1 {\n    color: light-dark(#000, #FFF);\n}"
    style_helptext = f"Format: {style_default}"

    name = models.CharField(max_length=64, blank=False, null=False)
    display_title = models.CharField(max_length=64, blank=False, null=False)
    order_val = models.IntegerField(default=100)
    type = models.CharField(max_length=1, choices=RANK_TYPE_CHOICES, blank=False, null=False, default="P")
    post_count = models.IntegerField(blank=True, null=True)
    style = models.TextField(blank=True, null=False, help_text=style_helptext, default=style_default)
    is_admin = models.BooleanField(blank=False, null=False, default=False)

    can_view = models.BooleanField(default=True)
    can_post = models.BooleanField(default=True)
    can_edit_own = models.BooleanField(default=True)
    can_edit_other = models.BooleanField(default=False)
    can_delete_own = models.BooleanField(default=False)
    can_delete_other = models.BooleanField(default=False)
    can_view_deleted = models.BooleanField(default=False)
    can_lock = models.BooleanField(default=False)
    can_post_locked = models.BooleanField(default=False)
    can_pin = models.BooleanField(default=False)
    can_mod = models.BooleanField(default=False)
    can_ban = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)

    @staticmethod
    def _make_cache():
        ranks_cache = {"P": {}, "M": []}
        for rank in Rank.objects.all():
            if rank.type == "G":
                ranks_cache["G"] = rank
            elif rank.type == "D":
                ranks_cache["P"][0] = rank
            elif rank.type == "M":
                ranks_cache["M"].append(rank)
            else:
                ranks_cache["P"][rank.post_count] = rank
        return ranks_cache

    def save(self, *args, **kwargs):
        if not self.style:
            self.style = self.style_default
        super().save(*args, **kwargs)
        for forum in Forum.objects.prefetch_related("permissions").all():
            try:
                perm_set = forum.permissions.get(rank=self.id)
                changed = False
                for field in perm_set.perm_fields:
                    if perm_set.use_default and getattr(perm_set, field) != getattr(self, field):
                        changed = True
                        setattr(perm_set, field, getattr(self, field))
                if changed:
                    perm_set.save()
            except ForumPermissionSet.DoesNotExist:
                ForumPermissionSet.objects.create(
                    forum_id=forum.id,
                    rank_id=self.id,
                    can_view=self.can_view,
                    can_post=self.can_post,
                    can_edit_own=self.can_edit_own,
                    can_edit_other=self.can_edit_other,
                    can_delete_own=self.can_delete_own,
                    can_delete_other=self.can_delete_other,
                    can_view_deleted=self.can_view_deleted,
                    can_lock=self.can_lock,
                    can_post_locked=self.can_post_locked,
                    can_pin=self.can_pin,
                    can_mod=self.can_mod,
                    can_ban=self.can_ban,
                )

        cache.set("ranks", Rank._make_cache(), None)
        for profile in UserProfile.objects.all().select_related("user").prefetch_related("user__posts"):
            self.recalculate_rank(profile)

    def recalculate_rank(self, profile):
        ranks = Rank.load()
        if self.type in ["M", "G"]:
            return
        post_count = profile.user.posts.count()
        new_rank = ranks["P"][[x for x in sorted(ranks["P"], key=lambda x: x) if x <= post_count][-1]]
        if new_rank != profile.rank:
            profile.rank = new_rank
            setattr(profile.user, "rank", new_rank)
            profile.save()

    @classmethod
    def load(cls):
        if cache.get("ranks") is None:
            cache.set("ranks", cls._make_cache(), None)
        return cache.get("ranks")

    def clean(self):
        super().clean()

        if self.type == "P" and not self.post_count:
            raise ValidationError("You must have a post count set for Assigned via Post Count.")

        if self.type == "P" and self.post_count == 0:
            raise ValidationError(
                "You cannot set a rank with a post count of zero- modify the 'Default' rank instead."
            )

        if self.type == "P" and self.post_count < 0:
            raise ValidationError("You must have a positive post count set.")

        default_rank = Rank.objects.get(type="D")
        guest_rank = Rank.objects.get(type="G")

        if self.type == "D" and self.id != default_rank.id:
            raise ValidationError("There can only be one default rank.")
        if self.type != "D" and self.id == default_rank.id:
            raise ValidationError("You cannot remove the default rank.")

        if self.type == "G" and self.id != guest_rank.id:
            raise ValidationError("There can only be one guest rank.")
        if self.type != "G" and self.id == guest_rank.id:
            raise ValidationError("You cannot remove the guest rank.")

        if self.is_admin and self.type in ["G", "D"]:
            raise ValidationError("A guest or default rank cannot be made admin.")

        admins = Rank.objects.filter(is_admin=True).exclude(id=self.id).count()
        if not self.is_admin and admins == 0:
            raise ValidationError("You cannot remove the last admin rank.")
        admins = Rank.objects.filter(is_admin=True).exclude(id=self.id).count()

    @classmethod
    def get_css(cls):
        if not getattr(cls, "mini_css", False):
            generated_css = ""
            for rank in cls.objects.exclude(style=""):
                style = rank.style.replace("$1", f"a.rankstyle-{rank.id},.rankstyle-{rank.id}")
                generated_css += style
            cls.mini_css = Theme.minify(generated_css)
        return cls.mini_css


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(_sender, instance, *args, **kwargs):
    if not hasattr(instance, "profile"):
        if instance.is_superuser:
            rank = Rank.objects.filter(is_admin=True)[0]
        else:
            rank = Rank.objects.get(type="D")
        UserProfile.objects.create(user=instance, rank=rank)
    is_staff = instance.is_staff
    is_admin = instance.profile.rank.is_admin
    admin_group = Group.objects.get(id=1)  # Admin Group ID
    if is_admin:
        if not is_staff:
            instance.is_staff = True
            instance.save()
        if not instance.groups.filter(id=1).exists():
            admin_group.user_set.add(instance)
    elif not is_admin:
        if is_staff:
            instance.is_staff = False
            instance.save()
        if instance.groups.filter(id=1).exists():
            admin_group.user_set.remove(instance)
    instance.profile.save()


@receiver(pre_save, sender=settings.AUTH_USER_MODEL)
def approval_activated(_sender, instance, *args, **kwargs):
    if not instance.id or instance.is_active is False or instance.last_login:
        return
    fc = ForumConfiguration.load()
    if not fc.new_user_approval:
        return
    original_active = settings.AUTH_USER_MODEL.objects.get(id=instance.id).is_active
    if original_active is False and instance.is_active is True:
        send_mail(
            subject=f"Your {fc.forum_name} account has been approved.",
            message=(
                f"Hello {instance.username},\nStaff has approved your account at {fc.forum_name}.\nYou may "
                f"now log in with your account and enjoy our community!\n\nThanks, {fc.forum_name} Staff"
            ),
            recipient_list=[instance.email],
        )


class ForumPermissionSet(models.Model):
    id = models.BigAutoField(primary_key=True)
    forum = models.ForeignKey(
        "weaverforum.forum",
        related_name="permissions",
        on_delete=models.CASCADE,
        null=True,
    )
    rank = models.ForeignKey("weaverforum.rank", related_name="permissions", on_delete=models.CASCADE)

    use_default = models.BooleanField(default=True)

    can_view = models.BooleanField(default=False)
    can_post = models.BooleanField(default=False)
    can_edit_own = models.BooleanField(default=False)
    can_edit_other = models.BooleanField(default=False)
    can_delete_own = models.BooleanField(default=False)
    can_delete_other = models.BooleanField(default=False)
    can_view_deleted = models.BooleanField(default=False)
    can_lock = models.BooleanField(default=False)
    can_post_locked = models.BooleanField(default=False)
    can_pin = models.BooleanField(default=False)
    can_mod = models.BooleanField(default=False)
    can_ban = models.BooleanField(default=False)

    def __str__(self):
        return ""

    @cached_property
    def perm_fields(self):
        return [
            f.name for f in self._meta.get_fields() if f.name not in ["use_default", "rank", "forum", "id"]
        ]

    def save(self, *args, **kwargs):
        for field in self.perm_fields:
            if self.use_default and getattr(self, field) != getattr(self.rank, field):
                setattr(self, field, getattr(self.rank, field))
        super().save(*args, **kwargs)


class PostReport(models.Model):
    id = models.BigAutoField(primary_key=True)
    REPORT_STATE_CHOICES = (
        ("O", "Open"),
        ("D", "Dismissed"),
        ("A", "Action Taken"),
    )
    REPORT_ACTION_CHOICES = (
        ("N", "No Action"),
        ("B", "Ban User"),
        ("D", "Delete Post"),
        ("E", "Edit Post"),
    )
    post = models.ForeignKey("weaverforum.post", related_name="reports", on_delete=models.PROTECT, null=False)
    report = models.CharField(max_length=64, null=False, blank=False)
    reported_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="reports", on_delete=models.PROTECT
    )
    reported_on = models.DateTimeField(auto_now_add=True)
    state = models.CharField(max_length=1, choices=REPORT_STATE_CHOICES, default="O")
    action = models.CharField(max_length=1, choices=REPORT_ACTION_CHOICES, default="N")
    action_reason = models.CharField(max_length=128, null=False, blank=True)

    def __str__(self):
        return str(self.post)
